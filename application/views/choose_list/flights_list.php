<header><img src="<?php echo asset_url(); ?>img/plane-icon.png">Flights List</header>
<div style="border: 1px solid #7bbeff; border-bottom: 3px solid #7bbeff;">
    <ul>
        <li>
            Price
            <span>
                <ul class="result-page-filter" >
                    <li id="safar-sort-price-asc">Ascending Price</li>
                    <li id="safar-sort-price-desc">Descend Price</li>
                </ul>
            </span>
        </li>
        <li>
            Airlines
            <span>
                <ul class="result-page-filter" id="flight-filter-airline">
                    <?php
                    if (count($air_lines) > 0)
                        foreach ($air_lines as $air) {
                            ?>
                            <li id="safar-flight-airline" data-code="<?= $air['code'] ?>"><?= $air['name']; ?></li>
                        <?php } ?>
                </ul>
            </span>
        </li>
        <li>
            Stops
            <span>
                <ul class="result-page-filter">
                    <li>Round trip</li>
                    <li>One way</li>
                    <li>Multiple destinations</li>
                </ul>
            </span>
        </li>
    </ul>
</div>
<div class="scrollbar scroll-style" id="safar-flight" >
    <?php
    if (count($flight) > 0) {
        foreach ($flight as $fl) {
            ?>
            <div class="result-page-result" id="safar-flight-result" data-departure-airport="<?=$fl['departureAirport']?>" data-arrival-airport="<?=$fl['arrivalAirport']?>" data-flight-number="<?= $fl['flightNumber'] ?>" data-airline-code="<?= $fl['airLineCode'] ?>" data-id="<?= $fl['tflight'] ?>">
                <span style="width: 70px; height: 80px; padding-top: 20px;">
                    From
                    <br>
                    <span id="safar-flight-price" data-value="<?= $fl['price'] ?>" style="width: 70px; color: #1fbc00; font-weight: 600;"><?= $fl['price'] ?>RI</span>
                    <br>
                </span>
                <span style="width: 100px; height: 80px; padding-top: 25px;">
                    <img src=<?= $fl['airLineLogo'] ?>>
                </span>
                <span id="safar-flight-departure-date" style="width: 100px; height: 80px; padding-top: 30px;" data-value="<?= $fl['departureDate'] ?>"><?= $fl['departureDate'] ?></span>
                <span style="width: 30px; height: 80px; padding-top: 30px;">
                    <img class="result-btn" src="<?php echo asset_url(); ?>img/plus-icon.png" data-swap="<?php echo asset_url(); ?>img/minus-icon.png">
                </span>
                <div class="clearfix"></div>
                <div class="result-page-result-detail result-page-toggle" id="safar-flight-result-detail">
                    <span id="safar-flight-time" data-arrival-date="<?= $fl['arrivalDate'] ?>" data-arrival-time="<?= $fl['arrivalTime'] ?>" data-departure-time="<?= $fl['departureTime'] ?>" style="width: 70px; height: 80px; padding-top: 20px;">
                        <?= $fl['departureTime'] ?>
                        <br>
                        <?= $fl['arrivalTime'] ?>
                        <br>
                    </span>
                    <span style="width: 100px; height: 80px; padding-top: 25px;">
                        <img src="<?= $fl['airLineLogo'] ?>">
                    </span>
                    <span id="safar-flight-length" data-flight-hour="<?= $fl['flightLengthHour'] ?>" data-flight-minute="<?= $fl['flightLengthMinute'] ?>" style="width: 80px; height: 80px; padding-top: 30px;"><?= $fl['flightLengthHour'] ?>h <?= $fl['flightLengthMinute'] ?>m</span>
                    <span style="width: 50px; height: 80px; padding-top: 20px; color: #7bbeff;">
                        Non
                        <br>
                        Stop
                        <br>
                    </span>
                    <div class="clearfix"></div>
                </div>
            </div>
            <?php
        }
    }
    ?>
</div>