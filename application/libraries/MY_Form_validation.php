<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MY_Form_validation
 *
 * @author win7
 */
class MY_Form_validation extends CI_Form_validation {

    protected $CI;

    function __construct() {
        parent::__construct();
        $this->CI = & get_instance();
        $this->CI->load->helper('url_helper');
    }

    function error($field = '', $prefix = '', $suffix = '') {
        if (!empty($this->_field_data[$field]) && $this->_field_data[$field]['is_array']) {
            return ($prefix ? $prefix : $this->_error_prefix)
                    . array_shift($this->_field_data[$field]['error'])
                    . ($suffix ? $suffix : $this->_error_suffix);
        }

        return parent::error($field, $prefix, $suffix);
    }

    public function validateCity($city) {
        $this->CI->load->model('City_model');
        $this->CI->load->database();
        $this->CI->form_validation->set_message('validateCity', "The %s {$city} is invalid. Enter A Valid City");
        return $this->CI->City_model->existCity($city) ? true : false;
    }

    public function validateDate($bDate) {
        $regex = '/^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/';
        if (preg_match($regex, $bDate)) {
            return true;
        } else {
            $this->CI->form_validation->set_message('validateDate', "The %s {$bDate} is invalid. Enter A Valid Date");
            return false;
        }
    }

    public function validateExpDate($value, $field) {
        $nationality = $this->CI->input->post($field);
        $expDates = $this->CI->input->post('p_exp_date');
        foreach ($expDates as $key => $date) {
            if ($date == $value) {
                $index = $key;
            }
        }
        if ($nationality[$index] == 25) {
            return true;
        }
        $regex = '/^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/';
        if (preg_match($regex, $value)) {
            return true;
        } else {
            $this->CI->form_validation->set_message('validateExpDate', "The %s {$value} is invalid. Enter A Valid Date");
            return false;
        }
    }

    public function validateNationalCode($value, $field) {
        $valid = true;
        $nationality = $this->CI->input->post($field);
        $nationalCodes = $this->CI->input->post('pub_id');
        foreach ($nationalCodes as $key => $code) {
            if ($code == $value) {
                $index = $key;
            }
        }
        if ($nationality[$index] != 25) {
            return true;
        }
        if (strlen($value) <> 10) {
            $valid = false;
        } else {
            $valueArray = str_split($value);
            $AllEq;
            foreach ($valueArray as $item => $value) {
                if ($valueArray[0] <> $value) {
                    $AllEq = false;
                    break;
                } else {
                    $AllEq = true;
                }
            }
            if ($AllEq == true)
                $valid = FALSE;
            $j = 10;
            $sum = 0;
            for ($i = 0; $i <= 8; $i++) {
                $sum +=((int) ($valueArray[$i])) * $j;
                --$j;
            }
            $divid = $sum % 11;
            if ($divid <= 2) {
                if ($valueArray[9] == $divid) {
                    //echo "کد ملی معتبر است";
                }
                $valid = false;
            } else {
                $divid1 = 11 - $divid;
                if ($valueArray[9] == $divid1) {
                    // echo "کد ملی معتبر است";
                } else {
                    $valid = false;
                }
            }
        }

        if ($valid == false) {
            $this->CI->form_validation->set_message('validateNationalCode', "The %s {$value} is invalid. Enter A Valid National Code");
            //$this->_field_data['pub_id']['error'][$index]="The %s {$value} is invalid. Enter A Valid National Code";
            //$this->CI->form_validation->_field_data['pub_id[]']['error']='dddddddddddd';
        }
        return $valid;
    }

    public function validatePassport($passport, $field) {
        $nationality = $this->CI->input->post($field);
        $passports = $this->CI->input->post('passport_id');
        foreach ($passports as $key => $pass) {
            if ($pass == $passport) {
                $index = $key;
            }
        }
        if ($nationality[$index] == 25) {
            return true;
        }
        if ($passport == '1234') {
            $this->CI->form_validation->set_message('validatePassport', "The %s {$passport} is invalid. Enter A Valid P Issue Id");
            return false;
        }
        return true;
    }

    public function validateMobile($mobile, $field) {
        if (preg_match('@0?9(10|11|12|13|14|15|16|17|18|19|01|02|30|33|35|36|37|38|39|31|32|34|20|21)[0-9]{7}@', $mobile)) {
            return TRUE;
        }
        $this->CI->form_validation->set_message('validateMobile', "The %s {$mobile} is invalid. Enter A Valid Mobile Number");
        return FALSE;
    }

    public function validateIssue($pIssuedId, $field) {
        $nationality = $this->CI->input->post($field);
        $issued = $this->CI->input->post('p_issued_id');
        foreach ($issued as $key => $issue) {
            if ($issue == $pIssuedId) {
                $index = $key;
            }
        }
        if ($nationality[$index] == 25) {
            return true;
        }
        if ($pIssuedId == '1234') {
            $this->CI->form_validation->set_message('validateIssue', "The %s {$pIssuedId} is invalid. Enter A Valid P Issue Id");
            return false;
        }
        return true;
    }

}
