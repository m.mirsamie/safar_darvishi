<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url_helper');
        $this->load->helper('utility_helper');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('city_model');
    }

    public function index() {
        $data['cities'] = $this->loadCities();
        $data['fromCity'] = '';
        $this->load->view('home/header', $data);
        $this->load->view('home/search', $data);
        $this->load->view('home/footer');
    }

    public function reserve() {
        $this->load->view('home/reserve');
    }

    public function loadCities() {
        $result = $this->city_model->getAllCities();
        return json_encode($result);
    }

    public function userLocation() {

        $this->load->library('geoplugin');
        $geoplugin = new Geoplugin();
        $geoplugin->locate();
        $this->load->library('geolocation');
        $geoLocation = new Geolocation();
        try {
            $result = $geoLocation->getAddress($geoplugin->latitude, $geoplugin->longitude);
            $city = $result['components'][1]['long_name'];
            if (!empty($geoplugin->countryName)) {
                $pos = strpos($geoplugin->countryName, ',');
                if ($pos !== FALSE){
                    $country = substr($geoplugin->countryName, 0, $pos);
                }
                else{
                    $country = $geoplugin->countryName;
                }
                //                $iata = $this->City_model->findIataByCity($city);
                //                if (!empty($iata['iata'])) {
                //                    $iata = '(' . implode($iata['iata'], ',') . ')';
                //                    $city = $city . ', ' . $country . ', ' . $iata;
                //                }
                $city = $city . ', ' . $country;
            }
        } catch (Exception $ex) {
            $city = '';
        }
        return $city;
    }

    public function test() {
        date_default_timezone_set("Asia/Tehran");
        $date = '2015-11-30 00:00';
        echo 'aaa' . strtotime($date);
    }

    public function insertIata() {
        $this->load->database();
        $query = 'select * from  airline_iata';
        $result = $this->db->query($query)->result();
        foreach ($result as $res) {
            $this->db->query('insert into iata (iata,airport) values("' . $res->iata . '","' . $res->name . '")');
            echo $res->iata;
            echo '<br/>';
        }
        print_r($result);
        die;

//        $row = 0;
//        //$data=[];
//        if (($handle = fopen("data.csv", "r")) !== FALSE) {
//            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
//                $num = count($data);
//                //echo "<p> $num fields in line $row: <br /></p>\n";
//                // for ($c = 0; $c < $num; $c++) {
//                //echo $data[3];
//                //echo '-------------------------------------------';
//                ///echo $data[4];
//                //echo '<br/>';
//                $data2[] = [
//                    'iata' => $data[3],
//                    'icao' => $data[4]
//                ];
//                $row++;
//
//                //  }
//            }
//            foreach ($data2 as $da) {
//                //$query=$this->db->select('id')->from('iata')->where('iata',$da['icao'])->get();
//                $query = $this->db->query('select id from iata where iata="' . $da['icao'] . '"');
//                if ($query->num_rows() > 0) {
//                    $result = $query->row();
//                    //print_r($result->id);
//                    //die;
//                    $this->db->query('update iata set iata2="' . $da['iata'] . '" where id=' . $result->id . '');
//                }
//            }
//            fclose($handle);
//        }
//        $data = json_decode(file_get_contents('https://dl.dropboxusercontent.com/u/3599489/Data%20Dumps/city_airport_codes.json'), true);
//        $country = array();
//        foreach ($data as $d) {
//
//            if (!in_array($d['country'], $country)) {
//                $country[] = [
//                    'en_name' => $d['country']
//                ];
//            }
//        }
//        $this->load->database();
//        for ($k = 0; $k < count($country); $k++) {
//            $query = $this->db->query('select * from country where en_name="' . $country[$k]['en_name'] . '"');
//            if ($query->num_rows() == 0)
//                $this->db->insert('country', $country[$k]);
//        }
    }

}
