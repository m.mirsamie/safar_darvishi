<!DOCTYPE html>
<html>
    <head>
        <title>Safar</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="icon" href="<?= asset_url() ?>/img/result-page-plane-icon.png" type="image/gif">
        <link href="<?= asset_url() ?>css/autocomplete.min.css" rel="stylesheet">
        <link href="<?= asset_url() ?>css/datepicker.min.css" rel="stylesheet">
        <link href="<?= asset_url() ?>css/styles.css" rel="stylesheet">
        <script src="<?= asset_url() ?>js/jquery-1.11.1.min.js"></script>
        <script src="<?= asset_url() ?>js/autocomplete.min.js"></script>
        <script src="<?= asset_url() ?>js/datepicker.min.js"></script>
        <!--<script language="JavaScript" src="http://www.geoplugin.net/javascript.gp" type="text/javascript"></script>-->

    </head>
    <body>
        <div class="wrapper">
            <div class="result-page-header"> <!-- result-page-header -->
                <div class="logo" style="float: left;"> <!-- logo -->
                    <span id="first-letter">S</span><span id="other-letters">afar</span>
                </div>
                <span>login</span>
                <img src="<?= asset_url() ?>img/login.png">
                <img style="width: 72px; height: 38px; margin: 4px 140px 0 5px;" src="<?= asset_url() ?>img/mylist-extra-icon.png">
                <span style="color: #178dff;">My List</span>
                <img src="<?= asset_url() ?>img/mylist-icon.png">
                <span style="position: absolute; top: -1px; right: 235px;"><a class="mylist-tour" style="text-decoration: none; font-size: 10px; color: #fff; font-weight: 600;" href="#">24</a></span>
                <span style="position: absolute; top: -1px; right: 260px;"><a class="mylist-hotel" style="text-decoration: none; font-size: 10px; color: #fff; font-weight: 600;" href="#">06</a></span></a>
                <span style="position: absolute; top: -1px; right: 285px;"><a id="safar-flight-list-number" class="mylist-flight" style="text-decoration: none; font-size: 10px; color: #fff; font-weight: 600;" href="#"><?= $flights_number ?></a></span></a>
                <div class="clearfix"></div>
                <div class="clearfix"></div>
