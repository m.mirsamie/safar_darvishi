<div class="result-page-box-content" style="margin: 0 25px;">
    <header><img src="<?php echo asset_url(); ?>img/hotel-icon.png">Hotels List</header>
    <div style="border: 1px solid #7bbeff; border-bottom: 3px solid #7bbeff;">
        <ul>
            <li>
                Stars
                <span>
                    <ul class="result-page-filter">
                        <li>One star</li>
                        <li>Two stars</li>
                        <li>Three stars</li>
                        <li>Four stars</li>
                        <li>Five stars</li>
                    </ul>
                </span>
            </li>
            <li>
                Names
                <span>
                    <ul>
                        <li>Darvishi Hotels</li>
                        <li>Azadi Hotels</li>
                        <li>Homa Hotels</li>
                    </ul>
                </span>
            </li>
        </ul>
    </div>
    <div class="scrollbar scroll-style">
        <div class="result-page-result">
            <span style="width: 70px; height: 80px; padding-top: 25px;">
                <img src="<?php echo asset_url(); ?>img/4stars-icon.png">
            </span>
            <span style="width: 100px; height: 80px; padding-top: 18px;">
                <img src="<?php echo asset_url(); ?>img/darvishi-logo.png">
            </span>
            <span style="width: 100px; height: 80px; padding-top: 30px;">10-15-2015</span>
            <span style="width: 30px; height: 80px; padding-top: 30px;"><img class="result-btn" src="<?php echo asset_url(); ?>img/plus-icon.png" data-swap="<?php echo asset_url(); ?>img/minus-icon.png"></span>
            <div class="clearfix"></div>
            <div class="result-page-result-detail result-page-toggle">
                <span style="width: 70px; height: 80px; padding-top: 30px; color: #1fbc00; font-weight: 600;">
                    1000$
                </span>
                <span style="width: 230px; height: 80px; padding-top: 30px;">
                    Penthouse Rooms
                </span>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="result-page-result">
            <span style="width: 70px; height: 80px; padding-top: 25px;">
                <img src="<?php echo asset_url(); ?>img/4stars-icon.png">
            </span>
            <span style="width: 100px; height: 80px; padding-top: 18px;">
                <img src="<?php echo asset_url(); ?>img/darvishi-logo.png">
            </span>
            <span style="width: 100px; height: 80px; padding-top: 30px;">10-15-2015</span>
            <span style="width: 30px; height: 80px; padding-top: 30px;"><img class="result-btn" src="<?php echo asset_url(); ?>img/plus-icon.png" data-swap="<?php echo asset_url(); ?>img/minus-icon.png"></span>
            <div class="clearfix"></div>
            <div class="result-page-result-detail result-page-toggle">
                <span style="width: 70px; height: 80px; padding-top: 30px; color: #1fbc00; font-weight: 600;">
                    1000$
                </span>
                <span style="width: 230px; height: 80px; padding-top: 30px;">
                    Penthouse Rooms
                </span>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="result-page-result">
            <span style="width: 70px; height: 80px; padding-top: 25px;">
                <img src="<?php echo asset_url(); ?>img/4stars-icon.png">
            </span>
            <span style="width: 100px; height: 80px; padding-top: 18px;">
                <img src="<?php echo asset_url(); ?>img/darvishi-logo.png">
            </span>
            <span style="width: 100px; height: 80px; padding-top: 30px;">10-15-2015</span>
            <span style="width: 30px; height: 80px; padding-top: 30px;"><img class="result-btn" src="<?php echo asset_url(); ?>img/plus-icon.png" data-swap="<?php echo asset_url(); ?>img/minus-icon.png"></span>
            <div class="clearfix"></div>
            <div class="result-page-result-detail result-page-toggle">
                <span style="width: 70px; height: 80px; padding-top: 30px; color: #1fbc00; font-weight: 600;">
                    1000$
                </span>
                <span style="width: 230px; height: 80px; padding-top: 30px;">
                    Penthouse Rooms
                </span>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="result-page-result">
            <span style="width: 70px; height: 80px; padding-top: 25px;">
                <img src="<?php echo asset_url(); ?>img/4stars-icon.png">
            </span>
            <span style="width: 100px; height: 80px; padding-top: 18px;">
                <img src="<?php echo asset_url(); ?>img/darvishi-logo.png">
            </span>
            <span style="width: 100px; height: 80px; padding-top: 30px;">10-15-2015</span>
            <span style="width: 30px; height: 80px; padding-top: 30px;"><img class="result-btn" src="<?php echo asset_url(); ?>img/plus-icon.png" data-swap="<?php echo asset_url(); ?>img/minus-icon.png"></span>
            <div class="clearfix"></div>
            <div class="result-page-result-detail result-page-toggle">
                <span style="width: 70px; height: 80px; padding-top: 30px; color: #1fbc00; font-weight: 600;">
                    1000$
                </span>
                <span style="width: 230px; height: 80px; padding-top: 30px;">
                    Penthouse Rooms
                </span>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="result-page-result">
            <span style="width: 70px; height: 80px; padding-top: 25px;">
                <img src="<?php echo asset_url(); ?>img/4stars-icon.png">
            </span>
            <span style="width: 100px; height: 80px; padding-top: 18px;">
                <img src="<?php echo asset_url(); ?>img/darvishi-logo.png">
            </span>
            <span style="width: 100px; height: 80px; padding-top: 30px;">10-15-2015</span>
            <span style="width: 30px; height: 80px; padding-top: 30px;"><img class="result-btn" src="<?php echo asset_url(); ?>img/plus-icon.png" data-swap="<?php echo asset_url(); ?>img/minus-icon.png"></span>
            <div class="clearfix"></div>
            <div class="result-page-result-detail result-page-toggle">
                <span style="width: 70px; height: 80px; padding-top: 30px; color: #1fbc00; font-weight: 600;">
                    1000$
                </span>
                <span style="width: 230px; height: 80px; padding-top: 30px;">
                    Penthouse Rooms
                </span>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="result-page-result">
            <span style="width: 70px; height: 80px; padding-top: 25px;">
                <img src="<?php echo asset_url(); ?>img/4stars-icon.png">
            </span>
            <span style="width: 100px; height: 80px; padding-top: 18px;">
                <img src="<?php echo asset_url(); ?>img/darvishi-logo.png">
            </span>
            <span style="width: 100px; height: 80px; padding-top: 30px;">10-15-2015</span>
            <span style="width: 30px; height: 80px; padding-top: 30px;"><img class="result-btn" src="<?php echo asset_url(); ?>img/plus-icon.png" data-swap="<?php echo asset_url(); ?>img/minus-icon.png"></span>
            <div class="clearfix"></div>
            <div class="result-page-result-detail result-page-toggle">
                <span style="width: 70px; height: 80px; padding-top: 30px; color: #1fbc00; font-weight: 600;">
                    1000$
                </span>
                <span style="width: 230px; height: 80px; padding-top: 30px;">
                    Penthouse Rooms
                </span>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>