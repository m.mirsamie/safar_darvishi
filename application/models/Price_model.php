<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of city_model
 *
 * @author win7
 */
class Price_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
    }

    public function setRules() {
        $passportRequired = $this->input->post('nationality[]') != 25 ? 'required|' : '';
        $nationalCodeRequired = $this->input->post('nationality[]') == 25 ? 'required|' : '';

        $rules = [
            [
                'field' => 'first_name[]',
                'label' => 'First Name',
                'rules' => 'required|alpha'
            ],
            [
                'field' => 'sur_name[]',
                'label' => 'Sur Name',
                'rules' => 'required|alpha'
            ],
            [
                'field' => 'b_date[]',
                'label' => 'b date',
                'rules' => 'required|validateDate'
            ],
            [
                'field' => 'pub_id[]',
                'label' => 'Pub Id',
                'rules' => 'validateNationalCode[nationality[]]',
            ],
            [
                'field' => 'passport_id[]',
                'label' => 'Passport Id',
                'rules' => 'validatePassport[nationality[]]'
            ],
            [
                'field' => 'p_issued_id[]',
                'label' => 'P Issued Id',
                'rules' => 'validateIssue[nationality[]]'
            ],
            [
                'field' => 'p_exp_date[]',
                'label' => 'P Exp Date',
                'rules' => 'validateExpDate[nationality[]]'
            ],
            [
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'required|valid_email'
            ],
            [
                'field' => 'mobile',
                'label' => 'Mobile',
                'rules' => 'required|validateMobile'
            ]
        ];

        return $rules;
    }

    public function sendPassengerInfoToDb($info, $refrenceId) {

        $data = [];
        for ($i = 0; $i < count($info['first_name']); $i++) {
            $birthDate = date('Y-m-d', strtotime($info['b_date'][$i]));
            $iso = $this->getNationality($info['nationality'][$i]);
            $data[] = [
                'fname' => '',
                'lname' => '',
                'fname_en' => $info['first_name'][$i],
                'lname_en' => $info['sur_name'][$i],
                'birthday' => $birthDate,
                'nationality' => $iso,
                'shomare_melli' => $info['pub_id'][$i],
                'gender' => $info['gender'][$i],
                'shomare_passport' => $info['passport_id'][$i],
                'passport_issue_country' => $info['p_issued_id'][$i],
                'passport_engheza' => $info['p_exp_date'][$i],
                'refrence_id' => $refrenceId
            ];
        }
        foreach ($data as $passenger) {
            $this->db->insert('ticket', $passenger);
            $id = $this->db->insert_id();
            $ids[] = $id;
        }

        return $ids;
    }

    public function insertTickets($ids, $tickets) {
        for ($i = 0; $i < count($ids); $i++) {
            $data = [
                'ticket_number' => $tickets[$i]
            ];
            $this->db->where('id', $ids[$i]);
            $this->db->update('ticket', $data);
        }
    }

    public function confirmBook($refrenceId) {
        $webService = new RestWebservice();
        $result = $webService->confirmBook($refrenceId);
        return $result;
    }

    public function sendPassengerInfoToWebservice($info) {

        $list = $this->session->userdata('flight_list');
        $webserviceData = [];
        for ($i = 0; $i < count($info['first_name']); $i++) {
            $birthDate = date('Y-m-d', strtotime($info['b_date'][$i]));
            $iso = $this->getNationality($info['nationality'][$i]);
            $namePrefix = $this->getNamePrefix($info['gender'][$i]);

            $passenger[] = [
                'fname' => '',
                'lname' => '',
                'fname_en' => $info['first_name'][$i],
                'lname_en' => $info['sur_name'][$i],
                'birthDate' => $birthDate,
                'nationality' => $iso,
                'nationalCode' => $info['pub_id'][$i],
                'gender' => $info['gender'][$i],
                'namePrefix' => $namePrefix,
                'passportId' => $info['passport_id'][$i],
                'passportIssueCountry' => $info['p_issued_id'][$i],
                'passportExpire' => $info['p_exp_date'][$i],
            ];
        }
        foreach ($list as $flight) {
            $flight['departure_time'] = str_replace('pm', '00', $flight['departure_time']);
            $flight['departure_time'] = str_replace('am', '00', $flight['departure_time']);
            $flight['departure_time'] = str_replace(' ', ':', $flight['departure_time']);

            $flight['arrival_time'] = str_replace('pm', '00', $flight['arrival_time']);
            $flight['arrival_time'] = str_replace('am', '00', $flight['arrival_time']);
            $flight['arrival_time'] = str_replace(' ', ':', $flight['arrival_time']);

            $webserviceData[] = [
                'tflight' => $flight['tflight'],
                'price' => $flight['price'],
                'departureDate' => $flight['departure_date'],
                'arrivalDate' => $flight['arrival_date'],
                'departureTime' => $flight['departure_time'],
                'arrivalTime' => $flight['arrival_time'],
                'flightNumber' => $flight['flightNumber'],
                'fromCityIata' => $flight['departureAirport'],
                'toCityIata' => $flight['arrivalAirport'],
                'passengerInfo' => $passenger,
                'email' => $this->input->post('email'),
                'mobile' => $this->input->post('mobile'),
                'address' => 'address',
                'tell' => 'tell',
                'desc' => 'description'
            ];
        }
        $webService = new RestWebservice();
        $result = $webService->book($webserviceData[0]);

        return $result;
    }

    public function getNationality($countryId) {
        $res = $this->db->select('iso3')
                ->from('country')
                ->where('id', $countryId)
                ->get()
                ->row();
        $iso = $res->iso3;
        return $iso;
    }

    public function getFlightData() {

        $childrenPrice = 0;
        $adultPrice = 0;
        $list = $this->session->userdata('flight_list');
        foreach ($list as $key => $flight) {
            $list[$key]['departure_date'] = date("D,M,d-Y", strtotime($flight['departure_date']));
            $list[$key]['arrival_date'] = date("D,M,d-Y", strtotime($flight['arrival_date']));
        }
        if ($this->input->post('child') && $this->input->post('adult')) {
            $childrenNumber = $this->input->post('child');
            $adultNumber = $this->input->post('adult');
            $this->session->set_userdata('child_number', $childrenNumber);
            $this->session->set_userdata('adult_number', $adultNumber);
        } else {
            $childrenNumber = $this->session->userdata('child_number');
            $adultNumber = $this->session->userdata('adult_number');
        }
        foreach ($list as $fl) {
            $childrenPrice+=0.5 * $fl['price'];
            $adultPrice+=$fl['price'];
        }
        $childrenSumPrice = $childrenNumber * $childrenPrice;
        $adultSumPrice = $adultNumber * $adultPrice;
        $data = [
            'countries' => $this->city_model->getAllCountries(),
            'flight_list' => $list,
            'childrenNumber' => $childrenNumber,
            'adultNumber' => $adultNumber,
            'childrenPrice' => $childrenPrice,
            'adultPrice' => $adultPrice,
            'childrenSumPrice' => $childrenSumPrice,
            'adultSumPrice' => $adultSumPrice,
            'totalPrice' => $childrenSumPrice + $adultSumPrice,
            'passengerNumber' => intval($childrenNumber) + intval($adultNumber)
        ];

        return $data;
    }

    public function getNamePrefix($gender) {
        return ($gender == 'M') ? 'Mr' : 'Mis';
    }

}
