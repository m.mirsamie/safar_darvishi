<div class="choose-page-search"> <!-- cp-search -->
    <?= form_open('/ChooseList/index', array('method' => 'post')) ?>
    <?= form_label('From :', '', array('id' => 'from-pos')) ?>
    <?= form_label('To :', '', array('id' => 'to-pos')) ?>
    <?= form_input(['class' => 'autocomplete', 'id' => 'from-city', 'name' => 'from_city', 'value' => $fromCity]) ?>
    <?= form_input(['class' => 'autocomplete', 'id' => 'to-city', 'name' => 'to_city', 'placeholder' => "Enter a country, city"]) ?>
    <?= form_input(array('name' => 'from_city_id', 'type' => 'hidden', 'id' => 'from-city-id')); ?>
    <?= form_input(array('name' => 'to_city_id', 'type' => 'hidden', 'id' => 'to-city-id')); ?>
    <div><span>Go</span></div>
</form>

<img src="<?= asset_url(); ?>img/choose-page-tour-icon.png">
<img src="<?= asset_url(); ?>img/choose-page-hotel-icon.png">
<img src="<?= asset_url(); ?>img/choose-page-plane-icon.png">
</div>