$(document).ready(function() {
    //alert("Your location is: " +geoplugin_city());
    if (geoplugin_city() != '')
        $('#from-city').val(geoplugin_city() + ', ' + geoplugin_countryName());
    else
        $('#from-city').val(geoplugin_countryName());

    var int;
    var time = setInterval(function() {
        var newCity = cities[Math.floor(Math.random() * cities.length)];
        $('#to-city').attr('placeholder', newCity);
        if (int === 0)
            clearInterval(time);
    }, 3000);
    $('.autocomplete').autoComplete({
        minChars: 2,
        source: function(term, suggest) {
            term = term.toLowerCase();
            var choices = cities; //load all cities from DB here
            var suggestions = [];
            for (i = 0; i < choices.length; i++)
                if (~choices[i].toLowerCase().indexOf(term))
                    suggestions.push(choices[i]);
            suggest(suggestions);
        },
        onSelect: function(event, ui) {
            research();
        }
    });
    var build_html = function(result) {
        var html = '';
        for (var i = 0; i < result.length; i++) {
            html += "<div class='result-page-result'>";
            html += "<span style='width: 70px; height: 80px; padding-top: 20px;'>From<br><span style='width: 70px; color: #1fbc00; font-weight: 600;'>" + result[i].price + "IR</span><br></span>";
            html += " <span style='width: 100px; height: 80px; padding-top: 25px;'><img src='" + result[i]['airLineLogo'] + "'></span>";
            html += "<span style='width: 100px; height: 80px; padding-top: 30px;'>" + result[i]['departureDate'] + "</span>";
            html += "<span style='width: 30px; height: 80px; padding-top: 30px;'> <img class='result-btn' src='" + asset_url + "img/plus-icon.png' data-swap='<?php echo asset_url(); ?>img/minus-icon.png'></span>";
            html += "<div class='clearfix'></div>";
            html += "<div class='result-page-result-detail result-page-toggle'><span style='width: 70px; height: 80px; padding-top: 20px;'>" + result[i]['departureTime'] + "<br>";
            html += result[i]['arrivalTime'];
            html += "<br></span><span style='width: 100px; height: 80px; padding-top: 25px;'><img src='" + result[i]['airLineLogo'] + "'></span>";
            html += "<span style='width: 80px; height: 80px; padding-top: 30px;'>" + result[i]['flightLengthHour'] + "h " + result[i]['flightLengthMinute'] + "m</span><span style='width: 50px; height: 80px; padding-top: 20px; color: #7bbeff;'> Non<br>Stop<br/></span>";
            html += "<div class='clearfix'></div></div></div>";

        }
        return html;
    };
//    var sort_result = function(a, b) {
//        return b.price - a.price;
//    };

    var sort_result = function(param, order) {
        flights.sort(function(a, b) {
            switch (param) {
                case 'price':
                    if (order === 'asc')
                        return  parseInt(a.price) - parseInt(b.price);
                    else
                        return parseInt(b.price) - parseInt(a.price);
                    break;
            }
        });
    };

    var research = function() {
        if (confirm('Do You Want Refresh Search Result?')) {
            var fromCity = $('#research-from-city').val();
            var toCity = $('#research-to-city').val();
            var fromDate = $('#research-from-date').val();
            $.ajax({
                type: 'POST',
                url: base_url + "ajax/research",
                data: {fromCity: fromCity, toCity: toCity, fromDate: fromDate}
            }).done(function(result) {
                //console.log(result);
                flights = JSON.parse(result);
                //result.sort(sort_result);
                $('.flight').html(build_html(flights));
//                $('.flight').html(result);   // load html with data from php
                $('.result-page-toggle').hide();
                $('.result-btn').click(function() {
                    $(this).parent().parent().find(".result-page-toggle").stop().toggle('slow');
                });
            });
        }
    };

    $(document).on('keyup', '#research-from-city,#research-to-city', function(e) {
        if (e.keyCode === 13) {
            research();
        }
    });
    $(document).on('click', '.autocomplete-suggestion.selected', function() {
        research();
    });
    $('#research-from-date').change(function(){
        research();
    });
    $('#sort-price-asc').click(function() {
        sort_result('price', 'asc');
        $('.flight').html(build_html(flights));
        $('.result-page-toggle').hide();
        $('.result-btn').click(function() {
            $(this).parent().parent().find(".result-page-toggle").stop().toggle('slow');
        });
    });
    $('#sort-price-desc').click(function() {
        sort_result('price', 'desc');
        $('.flight').html(build_html(flights));
        $('.result-page-toggle').hide();
        $('.result-btn').click(function() {
            $(this).parent().parent().find(".result-page-toggle").stop().toggle('slow');
        });
    });
});