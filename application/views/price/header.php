<!DOCTYPE html>
<html>
    <head>
        <title>Safar</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="icon" href="<?= asset_url() ?>/img/result-page-plane-icon.png" type="image/gif">
        <link href="<?= asset_url(); ?>css/styles.css" rel="stylesheet">
        <link href="<?= asset_url() ?>css/datepicker.min.css" rel="stylesheet">
        <script src="<?= asset_url(); ?>js/jquery-1.11.1.min.js"></script>
        <script src="<?= asset_url(); ?>js/datepicker.min.js"></script>
        <script src="<?= asset_url(); ?>js/price/scripts.js"></script>
    </head>
    <body>
        <div class="wrapper">
            <div class="result-page-header"> <!-- price-page-header -->
                <div class="logo" style="float: left;"> <!-- logo -->
                    <span id="first-letter">S</span><span id="other-letters">afar</span>
                </div>
                <span>login</span>
                <img src="<?= asset_url(); ?>img/login.png">
                <div class="clearfix"></div>
            </div>
            <div class="timeline"> <!-- timeline -->
                <span class="inc-opacity" style="top: -12px; left: 50px;">Choose list</span>
                <span class="inc-opacity" style="top: -12px; left: 335px;">Price</span>
                <span style="top: -12px; left: 565px;">Passenger Details</span>
                <span style="top: -12px; left: 860px;">Payment</span>
                <img class="inc-opacity" src="<?= asset_url(); ?>img/timeline-choose.png">
                <img class="inc-opacity" src="<?= asset_url(); ?>img/timeline-price.png">
                <img src="<?= asset_url(); ?>img/timeline-passenger.png">
                <img src="<?= asset_url(); ?>img/timeline-payment.png">
                <img src="<?= asset_url(); ?>img/timeline-end.png">
                <div class="clearfix"></div>
            </div>