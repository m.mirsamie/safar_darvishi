<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Resclient
 *
 * @author win7
 */
class RestWebservice {

    public static function sendRequest($url, $user, $pass, $data) {
        $options = array(
            'http' => array(
                'header' => "User: $user\r\nPassword: $pass\r\nContent-Type: application/xml\r\n",
                'method' => 'POST',
                'content' => $data
            ),
        );
        $context = stream_context_create($options);
        $out = file_get_contents($url, false, $context);
        return($out);
    }

//    function sendRequest($url, $headers, $req) {
//        $curlOptions = array(
//            CURLOPT_RETURNTRANSFER => TRUE,
//            CURLOPT_FOLLOWLOCATION => TRUE,
//            CURLOPT_VERBOSE => TRUE,
//            CURLOPT_STDERR => $verbose = fopen('php://temp', 'rw+'),
//            CURLOPT_FILETIME => TRUE,
//            CURLOPT_HTTPHEADER => $headers,
//            CURLOPT_POST => TRUE,
//            CURLOPT_POSTFIELDS => $req
//        );
//        $handle = curl_init($url);
//        curl_setopt_array($handle, $curlOptions);
//        $content = curl_exec($handle);
//        curl_close($handle);
//        return($content);
//    }

    function search($date, $from_city, $to_city) {
        //Safar Search Example
        $url = "http://185.55.225.69/rest/lowfaresearch";
        $User = 'arad';
        $Password = '123456';
        $req = '<OTA_AirLowFareSearchRQ xmlns="http://www.opentravel.org/OTA/2003/05"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xsi:schemaLocation="http://www.opentravel.org/OTA/2003/05 
                OTA_AirLowFareSearchRQ.xsd" EchoToken="1234" TimeStamp="2015-11-01T12:00:00+03:30"
                Target="Real" Version="2.001" SequenceNmbr="1" PrimaryLangID="En-us">
                      <OriginDestinationInformation>
                              <DepartureDateTime>' . $date . '</DepartureDateTime>
                              <OriginLocation LocationCode="' . $from_city . '"/>
                              <DestinationLocation LocationCode="' . $to_city . '"/>
                      </OriginDestinationInformation>
            </OTA_AirLowFareSearchRQ>';
        $content = $this->sendRequest($url, $User, $Password, $req);
        $xml = new SimpleXMLElement($content);
        $flights = $xml->PricedItineraries->PricedItinerary->AirItinerary->OriginDestinationOptions->OriginDestinationOption;
        return($flights);
    }

    function book($data) {

        $url = "http://185.55.225.69/rest/book";
        $User = 'arad';
        $Password = '123456';
        $req = '<OTA_AirBookRQ xmlns = "http://www.opentravel.org/OTA/2003/05"
        xmlns:xsi = "http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation = "http://www.opentravel.org/OTA/2003/05 
OTA_AirBookRQ.xsd" EchoToken = "50987" TimeStamp = "2015-11-02IRST16:06:12+03:30"
        Target = "Real" Version = "2.001" SequenceNmbr = "1" PrimaryLangID = "En-us">
        <AirItinerary DirectionInd = "OneWay">
        <OriginDestinationOptions>
        <OriginDestinationOption>
        <FlightSegment TFlight = "4d1134cdd2efc22ff1aa016538afb219" 
        FlightNumber = "969" 
        ResBookDesigCode = "Y" 
        DepartureDateTime = "' . $data['departureDate'] . 'T' . $data['departureTime'] . '.000+03:30"
        ArrivalDateTime = "' . $data['arrivalDate'] . 'T' . $data['arrivalTime'] . '.000+03:30" 
        StopQuantity = "0" RPH = "1">
        <DepartureAirport LocationCode = "' . $data['fromCityIata'] . '" Terminal = "T1"/>
        <ArrivalAirport LocationCode = "' . $data['toCityIata'] . '" Terminal = "T2"/>
        </FlightSegment>
        </OriginDestinationOption>
        </OriginDestinationOptions>
        </AirItinerary>
        <PriceInfo>
        <ItinTotalFare>
        <TotalFare CurrencyCode = "IRR" DecimalPlaces = "0" Amount = "' . $data['price'] . '"/>
        </ItinTotalFare>
        </PriceInfo>
        <TravelerInfo>';
        foreach ($data['passengerInfo'] as $passenger) {
            $req.='<AirTraveler PassengerTypeCode = "ADT" Gender = "M" BirthDate = "' . $passenger['birthDate'] . '" AccompaniedByInfantInd = "false">
        <PersonName>
        <NamePrefix>' . $passenger['namePrefix'] . '</NamePrefix>
        <GivenName>' . $passenger['fname_en'] . '</GivenName>
        <Surname>' . $passenger['lname_en'] . '</Surname>
        </PersonName>
        <Telephone PhoneNumber = "' . $data['tell'] . '"/>
        <Email>' . $data['email'] . '</Email>
        <Document DocID = "' . $passenger['passportId'] . '"  DocIssueCountry="' . $passenger['passportIssueCountry'] . '" ExpireDate="' . $passenger['passportExpire'] . '" DocHolderNationality = "' . $passenger['nationality'] . '"/>
        </AirTraveler>';
        }
        $req.='</TravelerInfo>
        </OTA_AirBookRQ>';
        $content = $this->sendRequest($url, $User, $Password, $req);
        $xml = new SimpleXMLElement($content);

        if (empty($xml->Errors)) {
            //echo 'success' . '<br/>';
            $refrenceId = $xml->AirReservation->BookingReferenceID->attributes()->ID;
            $return = [
                'result' => 'success',
                'refrenceId' => $refrenceId,
                'errors' => []
            ];
        } else {
            //echo 'error';
            foreach ((array) $xml->Errors as $index => $node) {
                foreach ($node as $key => $n) {
                    $code = (string) $n->attributes()->Code . '<br/>';
                    $msg = (string) $n->attributes()->Message;
                    $errors[] = [
                        'code' => $code,
                        'message' => $msg
                    ];
                }
            }
            $return = [
                'result' => 'failed',
                'refrenceId' => '',
                'errors' => $errors
            ];
        }
        return $return;
    }

    public function confirmBook($refrenceId) {

        $url = "http://185.55.225.69/rest/confirm";
        $User = 'arad';
        $Password = '123456';
        $req = '<OTA_AirBookConfirmRQ xmlns="http://www.opentravel.org/OTA/2003/05"
                      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                      xsi:schemaLocation="http://www.opentravel.org/OTA/2003/05 
                       OTA_AirBookRQ.xsd" EchoToken="50987" TimeStamp="2015-11-02IRST16:06:12+03:30"
                      Target="Real" Version="2.001" SequenceNmbr="1" PrimaryLangID="En-us">
                     <AirReservation CreatedDateTme="4564656">
                     <BookingReferenceID ID="' . $refrenceId . '" ID_Context="BookingRef"/>
                     </AirReservation>
                     </OTA_AirBookConfirmRQ>';
        $content = $this->sendRequest($url, $User, $Password, $req);

        $xml = new SimpleXMLElement($content);
        if (empty($xml->Errors)) {
            foreach ($xml->AirReservation->Ticketing as $ticket) {
                $tickets[] = $ticket->attributes()->TicketDocumentNbr;
                // echo '<br/>';
            }
            //die;
            $return = [
                'result' => 'success',
                'tickets' => $tickets,
                'errors' => []
            ];
        } else {

            foreach ((array) $xml->Errors as $index => $node) {
                foreach ($node as $key => $n) {
                    $code = (string) $n->attributes()->Code . '<br/>';
                    $msg = (string) $n->attributes()->Message;
                    $errors[] = [
                        'code' => $code,
                        'message' => $msg
                    ];
                }
            }
            $return = [
                'result' => 'failed',
                'tickets' => [],
                'errors' => $errors
            ];
        }
        return $return;
    }

}
