<!DOCTYPE html>
<html>
    <head>
        <title>Safar</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="icon" href="<?= asset_url() ?>/img/result-page-plane-icon.png" type="image/gif">
        <link href="<?= asset_url() ?>css/autocomplete.min.css" rel="stylesheet">
        <link href="<?= asset_url() ?>css/datepicker.min.css" rel="stylesheet">
        <link href="<?= asset_url() ?>css/styles.css" rel="stylesheet">
        <script src="<?= asset_url() ?>js/jquery-1.11.1.min.js"></script>
        <script src="<?= asset_url() ?>js/jquery-autocomplete.min.js"></script>
        <script src="<?= asset_url() ?>js/datepicker.js"></script>
        <script type="text/javascript">
            var cities =<?= $cities ?>;
        </script>
        <script src="<?= asset_url() ?>js/home/scripts.js"></script>
        <script language="JavaScript" src="http://www.geoplugin.net/javascript.gp" type="text/javascript"></script>

    </head>
    <body>
        <div class="wrapper">
            <div class="choose-page-header"> <!-- choose-page-header -->
                <span>login</span>
                <img src="<?= asset_url() ?>img/login.png">
                <img src="<?= asset_url() ?>img/map.png">
                <div class="clearfix"></div>
            </div>
            <div class="logo" style="margin: 10% auto 0px auto;"> <!-- logo -->
                <span id="first-letter">S</span><span id="other-letters">afar</span>
            </div>