$(document).ready(function() {
    $(".datepicker").datepicker({
        dateFormat: 'dd/mm/yy'
    });
    var spanSubmit = $('.choose-page-search div span');
    spanSubmit.on('click', function() {
        $(this).closest('form').submit();
    });
    //result-filter
    $('.result-page-box ul li span').hide();
    $('.result-page-box ul li').mouseover(function() {
        $(this).find('span').stop().slideDown('slow');
    });
    $('.result-page-box ul li').mouseout(function() {
        $(this).find('span').stop().slideUp('slow');
    });

    //result-detail
    $('.result-page-toggle').hide();
    $('.result-btn').click(function() {
        $(this).parent().parent().find(".result-page-toggle").stop().toggle('slow');
    });

    //result-detail-btn
    $('.result-btn').click(function() {
        var _this = $(this);
        var current = _this.attr("src");
        var swap = _this.attr("data-swap");
        _this.attr('src', swap).attr("data-swap", current);
    });
//    if (geoplugin_city() != '')
//        $('#from-city').val(geoplugin_city() + ', ' + geoplugin_countryName());
//    else
//        $('#from-city').val(geoplugin_countryName());

    $(document).on('keyup', '#to-city', function(e) {
        if (e.keyCode === 13)
            $(this).closest('form').submit();
    });
    var int;
    var time = setInterval(function() {
        var newCity = cities[Math.floor(Math.random() * cities.length)];
        $('#to-city').attr('placeholder', newCity);
        if (int === 0)
            clearInterval(time);
    }, 3000);
    $('.autocomplete').autoComplete({
        minChars: 2,
        source: function(term, suggest) {
            term = term.toLowerCase();
            var choices = cities; //load all cities from DB here
            var suggestions = [];
            for (i = 0; i < choices.length; i++)
                if (~choices[i].toLowerCase().indexOf(term))
                    suggestions.push(choices[i]);
            suggest(suggestions);
        },
    });
    var getLocation = function() {
        if (navigator.geolocation) {
            navigator.geolocation.watchPosition(showPosition, showError);
        } else {
        }
    };

    function showPosition(position) {
        $.getJSON('http://maps.googleapis.com/maps/api/geocode/json?latlng=' + position.coords.latitude + ',' + position.coords.longitude + '&SENSOR=TRUE&LANGUAGE=FA', function(data) {
            if (data.status === 'OK') {
                var country = data.results[0].address_components[5].long_name;
                //var state = data.results[0].address_components[4].long_name;
                var city = data.results[0].address_components[3].long_name;
                $('#from-city').val(city + ", " + country);
                /*$.ajax({
                 type: 'POST',
                 url: "ajax/findIataByCity",
                 data: {city: city}
                 }).done(function(result) {
                 var iata = JSON.parse(result).iata;
                 var city_id = JSON.parse(result).city_id;
                 $('#from-city').val(city + ", " + country + ", ( " + iata + ")");
                 //$('#from-city-id').val(city_id);
                 });*/
            }
        });
    }
    function showError(error) {
        if (error.code) {
            alert(error.code);
            var city = '';
            if (geoplugin_city() !== '')
                $('#from-city').val(geoplugin_city() + ', ' + geoplugin_countryName());
            else
                $('#from-city').val(geoplugin_countryName());
        }
    }
    getLocation();
});