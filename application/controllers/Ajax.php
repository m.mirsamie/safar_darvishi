<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->database();
        $this->load->model('city_model');
        $this->load->model('flight_model');
        $this->load->helper('url_helper');
        $this->load->library('form_validation');
        $this->load->helper('utility_helper');
        $this->load->library('session');
        $this->load->helper('cookie');
    }

    public function loadCities() {

        $result = $this->City_model->getAllCities();
        echo json_encode($result);
    }

    public function findIataByCity() {

        $cityName = $this->input->post('city');
        $iata = $this->City_model->findIataByCity($cityName);
        echo json_encode($iata);
    }

    public function research() {

        $fromDate = date('Y-m-d', strtotime($this->input->post('fromDate')));
        $toDate = date('Y-m-d', strtotime($this->input->post('toDate')));
//        echo $fromDate;
//        echo '<br/>';
//        echo $toDate;
        $fromCity = (strpos($this->input->post('fromCity'), ',') != false) ? substr($this->input->post('fromCity'), 0, strpos($this->input->post('fromCity'), ',')) : $this->input->post('fromCity');
        $toCity = (strpos($this->input->post('toCity'), ',') != false) ? substr($this->input->post('toCity'), 0, strpos($this->input->post('toCity'), ',')) : $this->input->post('toCity');
        $cities = [
            'from_city' => $fromCity,
            'to_city' => $toCity
        ];
        $rules = $this->flight_model->setRules();
        $this->form_validation->set_data($cities);
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE) {
            echo 'false';
            //redirect('/');
        } else {
            $fromCityIata = $this->city_model->findIataByCity($fromCity);
            $toCityIata = $this->city_model->findIataByCity($toCity);
            $result1 = $this->flight_model->search($fromCityIata, $toCityIata, $fromDate);
            if ($fromDate != $toDate) {
                $result2 = $this->flight_model->search($fromCityIata, $toCityIata, $toDate);
                $result = [
                    'info' => array_merge($result1['info'], $result2['info']),
                    'airLines' => array_unique(array_merge($result1['airLines'], $result2['airLines']), SORT_REGULAR)
                ];
            } else {
                $result = $result1;
            }
            $data = [
                'flight' => $result['info'],
                'air_lines' => $result['airLines']
            ];
            $this->load->view('choose_list/flights_list', $data);
            //$flightList =$this->load->view('choose_list/flights_list', $data, true);
//            $result = [
//                'flightList' => $flightList
//            ];
//            //print_r($result);die;
//            echo json_encode($result, JSON_HEX_QUOT | JSON_HEX_TAG);
        }
    }

    public function saveFlightList() {
        //$this->session->unset_userdata('flight_list');
        $type = $this->input->post('type');
        $data = $this->input->post('data');
        $this->flight_model->saveFlightList($type, $data);
//        print_r($this->session->userdata('flight_list'));
//        $cookie = [
//            'name' => 'flight_list',
//            'value' => json_encode($result)
//        ];
//        set_cookie($cookie);
    }

    public function validateFlightList() {
        if (!$this->session->userdata('flight_list')) {
            $return = [
                'result' => 'false',
                'msg' => 'Your Flight List Is Empty.'
            ];
        } else {
            $list = $this->session->userdata('flight_list');
            if ($this->flight_model->arrayUniqueFromKey($list) == 'true') {
                $return = [
                    'result' => 'false',
                    'msg' => 'In Your Flight List Exist Flights With Common Source And Destination In One Date.'
                ];
            } else {
                $return = [
                    'result' => 'true'
                ];
            }
        }
        echo json_encode($return);
    }

}
