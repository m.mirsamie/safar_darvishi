<div class="result-page-box-content">
    <header><img src="<?php echo asset_url(); ?>img/tour-icon.png">Tours List</header>
    <div style="border: 1px solid #7bbeff; border-bottom: 3px solid #7bbeff;">
        <ul>
            <li>
                Price
                <span>
                    <ul class="result-page-filter">
                            <li>Ascending Price</li>
                            <li>Descending Price</li>
                    </ul>
                </span>
            </li>
            <li>
                Days/Nights
                <span></span>
            </li>
        </ul>
    </div>
    <div class="scrollbar scroll-style">
        <div class="result-page-result">
            <span style="width: 70px; height: 80px; padding-top: 20px;">
                From
                <br>
                <span style="color: #7bbeff; font-weight: 600;">250$</span>
                <br>
            </span>
            <span style="width: 100px; height: 80px; padding-top: 20px;">
                3 Days
                <br>
                4 Nights
                <br>
            </span>
            <span style="width: 100px; height: 80px; padding-top: 30px;">10-15-2015</span>
            <span style="width: 30px; height: 80px; padding-top: 30px;"><img class="result-btn" src="<?php echo asset_url(); ?>img/plus-icon.png" data-swap="<?php echo asset_url(); ?>img/minus-icon.png"></span>
            <div class="clearfix"></div>
            <div class="result-page-result-detail result-page-toggle">
                <span style="width: 150px; height: 80px; padding-top: 30px; color: #1fbc00; font-weight: 600;">
                    Adibian Agency
                </span>
                <span style="width: 150px; height: 80px; padding-top: 30px; color: #7bbeff;">
                    Ghasr Hotel
                </span>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="result-page-result">
            <span style="width: 70px; height: 80px; padding-top: 20px;">
                From
                <br>
                <span style="color: #7bbeff; font-weight: 600;">250$</span>
                <br>
            </span>
            <span style="width: 100px; height: 80px; padding-top: 20px;">
                3 Days
                <br>
                4 Nights
                <br>
            </span>
            <span style="width: 100px; height: 80px; padding-top: 30px;">10-15-2015</span>
            <span style="width: 30px; height: 80px; padding-top: 30px;"><img class="result-btn" src="<?php echo asset_url(); ?>img/plus-icon.png" data-swap="<?php echo asset_url(); ?>img/minus-icon.png"></span>
            <div class="clearfix"></div>
            <div class="result-page-result-detail result-page-toggle">
                <span style="width: 130px; height: 80px; padding-top: 30px; color: #1fbc00; font-weight: 600;">
                    Adibian Agency
                </span>
                <span style="width: 170px; height: 80px; padding-top: 30px; color: #7bbeff;">
                    Ghasr Hotel
                </span>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="result-page-result">
            <span style="width: 70px; height: 80px; padding-top: 20px;">
                From
                <br>
                <span style="color: #7bbeff; font-weight: 600;">250$</span>
                <br>
            </span>
            <span style="width: 100px; height: 80px; padding-top: 20px;">
                3 Days
                <br>
                4 Nights
                <br>
            </span>
            <span style="width: 100px; height: 80px; padding-top: 30px;">10-15-2015</span>
            <span style="width: 30px; height: 80px; padding-top: 30px;"><img class="result-btn" src="<?php echo asset_url(); ?>img/plus-icon.png" data-swap="<?php echo asset_url(); ?>img/minus-icon.png"></span>
            <div class="clearfix"></div>
            <div class="result-page-result-detail result-page-toggle">
                <span style="width: 170px; height: 80px; padding-top: 30px; color: #1fbc00; font-weight: 600;">
                    Adibian Agency
                </span>
                <span style="width: 130px; height: 80px; padding-top: 30px; color: #7bbeff;">
                    Ghasr Hotel
                </span>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="result-page-result">
            <span style="width: 70px; height: 80px; padding-top: 20px;">
                From
                <br>
                <span style="color: #7bbeff; font-weight: 600;">250$</span>
                <br>
            </span>
            <span style="width: 100px; height: 80px; padding-top: 20px;">
                3 Days
                <br>
                4 Nights
                <br>
            </span>
            <span style="width: 100px; height: 80px; padding-top: 30px;">10-15-2015</span>
            <span style="width: 30px; height: 80px; padding-top: 30px;"><img class="result-btn" src="<?php echo asset_url(); ?>img/plus-icon.png" data-swap="<?php echo asset_url(); ?>img/minus-icon.png"></span>
            <div class="clearfix"></div>
            <div class="result-page-result-detail result-page-toggle">
                <span style="width: 130px; height: 80px; padding-top: 30px; color: #1fbc00; font-weight: 600;">
                    Adibian Agency
                </span>
                <span style="width: 170px; height: 80px; padding-top: 30px; color: #7bbeff;">
                    Ghasr Hotel
                </span>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="result-page-result">
            <span style="width: 70px; height: 80px; padding-top: 20px;">
                From
                <br>
                <span style="color: #7bbeff; font-weight: 600;">250$</span>
                <br>
            </span>
            <span style="width: 100px; height: 80px; padding-top: 20px;">
                3 Days
                <br>
                4 Nights
                <br>
            </span>
            <span style="width: 100px; height: 80px; padding-top: 30px;">10-15-2015</span>
            <span style="width: 30px; height: 80px; padding-top: 30px;"><img class="result-btn" src="<?php echo asset_url(); ?>img/plus-icon.png" data-swap="<?php echo asset_url(); ?>img/minus-icon.png"></span>
            <div class="clearfix"></div>
            <div class="result-page-result-detail result-page-toggle">
                <span style="width: 130px; height: 80px; padding-top: 30px; color: #1fbc00; font-weight: 600;">
                    Adibian Agency
                </span>
                <span style="width: 170px; height: 80px; padding-top: 30px; color: #7bbeff;">
                    Ghasr Hotel
                </span>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="result-page-result">
            <span style="width: 70px; height: 80px; padding-top: 20px;">
                From
                <br>
                <span style="color: #7bbeff; font-weight: 600;">250$</span>
                <br>
            </span>
            <span style="width: 100px; height: 80px; padding-top: 20px;">
                3 Days
                <br>
                4 Nights
                <br>
            </span>
            <span style="width: 100px; height: 80px; padding-top: 30px;">10-15-2015</span>
            <span style="width: 30px; height: 80px; padding-top: 30px;"><img class="result-btn" src="<?php echo asset_url(); ?>img/plus-icon.png" data-swap="<?php echo asset_url(); ?>img/minus-icon.png"></span>
            <div class="clearfix"></div>
            <div class="result-page-result-detail result-page-toggle">
                <span style="width: 130px; height: 80px; padding-top: 30px; color: #1fbc00; font-weight: 600;">
                    Adibian Agency
                </span>
                <span style="width: 170px; height: 80px; padding-top: 30px; color: #7bbeff;">
                    Ghasr Hotel
                </span>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>