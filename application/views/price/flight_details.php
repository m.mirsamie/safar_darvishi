<div class="price-page-flight-detail"> <!-- flight-detail -->
    <header><img src="<?= asset_url(); ?>img/flight-detail-icon.png"> Your Flight Details</header>
    <?php foreach ($flight_list as $fl) { ?>
        <div style="border: 1px solid #1fbc00"> <!-- go-flight -->
            <div class="price-page-flight-detail-sub-header">
                <img src="<?= asset_url(); ?>img/go-icon.png">
                <span style="color: #009cff; padding-right: 25px;"><?= $fl['departure_date'] ?></span>
                <span style="color: #009cff"><?= $fl['from_city'] ?> to</span>
                <span style="color: #1fbc00"><?= $fl['to_city'] ?></span>
            </div>
            <table>
                <thead>
                    <tr>
                        <td>Depart :</td>
                        <td>Arrive :</td>
                        <td>Travel Time :</td>
                        <td><b>Flight Distance :</b></td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><span style="color: #009cff"><?= $fl['departure_time'] ?></span></td>
                        <td><span style="color: #009cff"><?= $fl['arrival_time'] ?> +1 Day</span></td>
                        <td><?= $fl['length_hour'] ?> hr and <?= $fl['length_minute'] ?> min</td>
                        <td>414 miles</td>
                    </tr>
                    <tr>
                        <td><span style="color: #009cff"><?= $fl['departure_date'] ?></span></td>
                        <td><span style="color: #009cff"><?= $fl['arrival_date'] ?></span></td>
                    </tr>
                    <tr>
                        <td><?= $fl['from_city'] ?></td>
                        <td><?= $fl['to_city'] ?></td>
                    </tr>
                </tbody>
            </table>
            <div class="price-page-flight-info">
                <span>Flight : </span>
                UA455
                <br>
                <span>Aircraft : </span>
                Boeing 737-900
                <br>
                <span>Fare Class : </span>
                United Economy (I)
                <br>
                <span>Meal : </span>
                None
                <br>
            </div>
            <div class="clearfix"></div>
        </div>
    <?php } ?> 

    <!--    <div style="border: 1px solid #1fbc00; margin-top: 10px;">  return-flight 
            <div class="price-page-flight-detail-sub-header">
                <img src="<?= asset_url(); ?>img/return-icon.png">
                <span style="color: #009cff; padding-right: 25px;">Tue,Nov.24-2015</span>
                <span style="color: #009cff">Las Vegas, NV, US(LAS) to</span>
                <span style="color: #1fbc00">San Francisco, CA, US(SFO)</span>
            </div>
            <table>
                <thead>
                    <tr>
                        <td>Depart :</td>
                        <td>Arrive :</td>
                        <td>Travel Time :</td>
                        <td><b>Flight Distance :</b></td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><span style="color: #009cff">10:57 pm</span></td>
                        <td><span style="color: #009cff">12:33 am +1 Day</span></td>
                        <td>1 hr and 36 min</td>
                        <td>414 miles</td>
                    </tr>
                    <tr>
                        <td><span style="color: #009cff">Fri, Nov.20-2015</span></td>
                        <td><span style="color: #009cff">Sat, Nov.20-2015</span></td>
                    </tr>
                    <tr>
                        <td>San Francisco, CA, US(SFO)</td>
                        <td>Las Vegas, NV, US(LAS)</td>
                    </tr>
                </tbody>
            </table>
            <div class="price-page-flight-info">
                <span>Flight : </span>
                UA455
                <br>
                <span>Aircraft : </span>
                Boeing 737-900
                <br>
                <span>Fare Class : </span>
                United Economy (I)
                <br>
                <span>Meal : </span>
                None
                <br>
            </div>
            <div class="clearfix"></div>
        </div>-->
</div>