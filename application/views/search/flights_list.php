<div class="result-page-box-content">
    <header><img src="<?php echo asset_url(); ?>img/plane-icon.png">Flights List</header>
    <ul>
        <li>
            Price
            <span>
                <form method="post">
                    <div class="form-vertical">
                        <button type="submit">Ascending Price</button>
                    </div>
                    <div class="form-vertical">
                        <button type="submit">Descending Price</button>
                    </div>
                </form>
            </span>
        </li>
        <li>
            Airlines
            <span>
                <form method="post">
                    <div class="form-vertical">
                        <button type="submit">Meraj</button>
                    </div>
                    <div class="form-vertical">
                        <button type="submit">Aseman</button>
                    </div>
                    <div class="form-vertical">
                        <button type="submit">Mahan</button>
                    </div>
                    <div class="form-vertical">
                        <button type="submit">Homa</button>
                    </div>
                </form>
            </span>
        </li>
        <li>
            Stops
            <span>
                <form method="post">
                    <div class="form-vertical">
                        <button type="submit">Roundtrip</button>
                    </div>
                    <div class="form-vertical">
                        <button type="submit">One way</button>
                    </div>
                    <div class="form-vertical">
                        <button type="submit">Multiple destinations</button>
                    </div>
                </form>
            </span>
        </li>
    </ul>
    <div class="scrollbar" class="scroll-style">
        <?php 
        if(count($flight)>0)
        foreach ($flight as $fl) { ?>
            <div class="result-page-result">
                <span style="width: 70px; height: 80px; padding-top: 20px;">
                    From
                    <br>
                    <span style="width: 70px; color: #1fbc00; font-weight: 600;"><?= $fl['price'] ?>$</span>
                    <br>
                </span>
                <span style="width: 100px; height: 80px; padding-top: 25px;">
                    <img src="<?php echo asset_url(); ?>img/meraj-logo.png">
                </span>
                <span style="width: 100px; height: 80px; padding-top: 30px;"><?= $fl['departureDate'] ?></span>
                <span style="width: 30px; height: 80px; padding-top: 30px;">
                    <img class="result-btn" src="<?php echo asset_url(); ?>img/plus-icon.png" data-swap="<?php echo asset_url(); ?>img/minus-icon.png">
                </span>
                <div class="clearfix"></div>
                <div class="result-page-result-detail result-page-toggle">
                    <span style="width: 70px; height: 80px; padding-top: 20px;">
                        <?= $fl['departureTime'] ?>
                        <br>
                        <?= $fl['arrivalTime'] ?>
                        <br>
                    </span>
                    <span style="width: 100px; height: 80px; padding-top: 25px;">
                        <img src="<?php echo asset_url(); ?>img/meraj-logo.png">
                    </span>
                    <span style="width: 80px; height: 80px; padding-top: 30px;"><?= $fl['flightLengthHour'] ?>h <?= $fl['flightLengthMinute'] ?>m</span>
                    <span style="width: 50px; height: 80px; padding-top: 20px; color: #7bbeff;">
                        Non
                        <br>
                        Stop
                        <br>
                    </span>
                    <div class="clearfix"></div>
                </div>
            </div>
        <?php } ?>
        <!--        <div class="result-page-result">
                    <span style="width: 70px; height: 80px; padding-top: 20px;">
                        From
                        <br>
                        <span style="width: 70px; color: #1fbc00; font-weight: 600;">250$</span>
                        <br>
                    </span>
                    <span style="width: 100px; height: 80px; padding-top: 25px;">
                        <img src="<?php echo asset_url(); ?>img/meraj-logo.png">
                    </span>
                    <span style="width: 100px; height: 80px; padding-top: 30px;">10-15-2015</span>
                    <span style="width: 30px; height: 80px; padding-top: 30px;">
                        <img class="result-btn" src="<?php echo asset_url(); ?>img/plus-icon.png" data-swap="<?php echo asset_url(); ?>img/minus-icon.png">
                    </span>
                    <div class="clearfix"></div>
                    <div class="result-page-result-detail result-page-toggle">
                        <span style="width: 70px; height: 80px; padding-top: 20px;">
                            8:05 am
                            <br>
                            9:15 am
                            <br>
                        </span>
                        <span style="width: 100px; height: 80px; padding-top: 25px;">
                            <img src="<?php echo asset_url(); ?>img/meraj-logo.png">
                        </span>
                        <span style="width: 80px; height: 80px; padding-top: 30px;">1h 10m</span>
                        <span style="width: 50px; height: 80px; padding-top: 20px;">
                            Non
                            <br>
                            Stop
                            <br>
                        </span>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="result-page-result">
                    <span style="width: 70px; height: 80px; padding-top: 20px;">
                        From
                        <br>
                        <span style="width: 70px; color: #1fbc00; font-weight: 600;">250$</span>
                        <br>
                    </span>
                    <span style="width: 100px; height: 80px; padding-top: 25px;">
                        <img src="<?php echo asset_url(); ?>img/meraj-logo.png">
                    </span>
                    <span style="width: 100px; height: 80px; padding-top: 30px;">10-15-2015</span>
                    <span style="width: 30px; height: 80px; padding-top: 30px;">
                        <img class="result-btn" src="<?php echo asset_url(); ?>img/plus-icon.png" data-swap="<?php echo asset_url(); ?>img/minus-icon.png">
                    </span>
                    <div class="clearfix"></div>
                    <div class="result-page-result-detail result-page-toggle">
                        <span style="width: 70px; height: 80px; padding-top: 20px;">
                            8:05 am
                            <br>
                            9:15 am
                            <br>
                        </span>
                        <span style="width: 100px; height: 80px; padding-top: 25px;">
                            <img src="<?php echo asset_url(); ?>img/meraj-logo.png">
                        </span>
                        <span style="width: 80px; height: 80px; padding-top: 30px;">1h 10m</span>
                        <span style="width: 50px; height: 80px; padding-top: 20px;">
                            Non
                            <br>
                            Stop
                            <br>
                        </span>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="result-page-result">
                    <span style="width: 70px; height: 80px; padding-top: 20px;">
                        From
                        <br>
                        <span style="width: 70px; color: #1fbc00; font-weight: 600;">250$</span>
                        <br>
                    </span>
                    <span style="width: 100px; height: 80px; padding-top: 25px;">
                        <img src="<?php echo asset_url(); ?>img/meraj-logo.png">
                    </span>
                    <span style="width: 100px; height: 80px; padding-top: 30px;">10-15-2015</span>
                    <span style="width: 30px; height: 80px; padding-top: 30px;">
                        <img class="result-btn" src="<?php echo asset_url(); ?>img/plus-icon.png" data-swap="<?php echo asset_url(); ?>img/minus-icon.png">
                    </span>
                    <div class="clearfix"></div>
                    <div class="result-page-result-detail result-page-toggle">
                        <span style="width: 70px; height: 80px; padding-top: 20px;">
                            8:05 am
                            <br>
                            9:15 am
                            <br>
                        </span>
                        <span style="width: 100px; height: 80px; padding-top: 25px;">
                            <img src="<?php echo asset_url(); ?>img/meraj-logo.png">
                        </span>
                        <span style="width: 80px; height: 80px; padding-top: 30px;">1h 10m</span>
                        <span style="width: 50px; height: 80px; padding-top: 20px;">
                            Non
                            <br>
                            Stop
                            <br>
                        </span>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="result-page-result">
                    <span style="width: 70px; height: 80px; padding-top: 20px;">
                        From
                        <br>
                        <span style="width: 70px; color: #1fbc00; font-weight: 600;">250$</span>
                        <br>
                    </span>
                    <span style="width: 100px; height: 80px; padding-top: 25px;">
                        <img src="<?php echo asset_url(); ?>img/meraj-logo.png">
                    </span>
                    <span style="width: 100px; height: 80px; padding-top: 30px;">10-15-2015</span>
                    <span style="width: 30px; height: 80px; padding-top: 30px;">
                        <img class="result-btn" src="<?php echo asset_url(); ?>img/plus-icon.png" data-swap="<?php echo asset_url(); ?>img/minus-icon.png">
                    </span>
                    <div class="clearfix"></div>
                    <div class="result-page-result-detail result-page-toggle">
                        <span style="width: 70px; height: 80px; padding-top: 20px;">
                            8:05 am
                            <br>
                            9:15 am
                            <br>
                        </span>
                        <span style="width: 100px; height: 80px; padding-top: 25px;">
                            <img src="<?php echo asset_url(); ?>img/meraj-logo.png">
                        </span>
                        <span style="width: 80px; height: 80px; padding-top: 30px;">1h 10m</span>
                        <span style="width: 50px; height: 80px; padding-top: 25px;">
                            Non
                            <br>
                            Stop
                            <br>
                        </span>
                        <div class="clearfix"></div>
                    </div>
                </div>-->
    </div>
</div>