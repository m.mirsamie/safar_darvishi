<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of City_model
 *
 * @author win7
 */
class City_model extends CI_Model {

    private $iata;
    private $name;
    private $en_name;

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
        $this->load->database();
    }

    public function getAllCountries() {

        $query = 'select id,en_name from country order by case when en_name = "Iran" then 1 else 2 end, en_name asc ';
        //$result = $this->db->query($query)->result();
        $stmt = $this->db->conn_id->prepare($query);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

    public function getAllCities() {

//        $this->db->select('city.id as city_id,city.en_name city,GROUP_CONCAT(iata.iata SEPARATOR " , ") iata,country.en_name country')
//                ->from('city')
//                ->where('country.id',25)
//               ->join('iata', 'city.id=iata.city_id', 'left')
//                ->join('country', 'city.country_id=country.id', 'left')
//                ->group_by('city.id')
//                ->order_by('country', 'asc');

        $this->db->select('city.id as city_id,city.en_name city,country.en_name country')
                ->from('city')
                // ->where('country.id',25)
                ->join('country', 'city.country_id=country.id', 'left')
                ->order_by('country', 'asc');
        $result = $this->db->get()->result_array();
//         $query='select city.id as city_id,city.en_name city,country.en_name country '
//          . ' from city left join country on city.country_id=country.id'
//          . ' order by country asc' ; 
        //echo $query;
        foreach ($result as $res) {
            $row = $res['city'] . ', ' . $res['country'];
            //if (!empty($res['iata'])) {
            //    $row.= ', ( ' . $res['iata'] . ' ) ';
            // }
            $rows[] = $row;
        }
        return $rows;
    }

    public function findIataByCity($cityName) {
        $this->db->select('iata.city_id,iata.icao')
                ->from('iata')
                ->join('city', 'city.id=iata.city_id')
                ->where('city.en_name', $cityName);
        $result = $this->db->get()->result();
        foreach ($result as $res) {
            $iata[] = $res->icao;
            //$iata['city_id'][] = $res->city_id;
        }
        return $iata;
    }

    public function existCity($city) {
        $this->db->select('id')
                ->from('city')
                ->where('en_name', $city);
        $result = $this->db->get()->num_rows();
        if ($result === 0) {
            return false;
        }
        return true;
    }

}
