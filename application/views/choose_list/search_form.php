<div class="result-page-search"> <!-- result-page-search -->
    <?= form_open('/choose_list/index', array('method' => 'post')) ?>
    <?= form_label('From : ') ?>
    <?= form_input(['class' => 'autocomplete', 'id' => 'research-from-city', 'name' => 'from_city', 'value' => $from_city]) ?>
    <?= form_label('To : ', '', array('style' => 'margin-left: 25px')) ?>
    <?= form_input(['class' => 'autocomplete', 'id' => 'research-to-city', 'name' => 'to_city', 'value' => $to_city]) ?>
    <?= form_input(['class' => 'datepicker', 'id' => 'research-from-date', 'name' => 'from_date', 'value' => date('Y/m/d'), 'style' => 'margin-left:40px']) ?>
    <?= form_input(['class' => 'datepicker', 'id' => 'research-to-date', 'name' => 'to_date', 'value' => date('Y/m/d')]) ?>
</form>
</div>
<script type="text/javascript">
    var cities =<?= $cities ?>;
    var base_url ="<?= base_url() ?>";
    var asset_url = "<?= asset_url() ?>";
</script>	
<script type="text/javascript" src="<?= asset_url() ?>js/choose_list/scripts.js"></script>