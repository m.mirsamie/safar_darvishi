<?php
$basePath = "";
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Safar</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="<?php echo $basePath; ?>css/autocomplete.min.css" rel="stylesheet">
        <link href="<?php echo $basePath; ?>css/datepicker.min.css" rel="stylesheet">
        <link href="<?php echo $basePath; ?>css/styles.css" rel="stylesheet">
        <script src="<?php echo $basePath; ?>js/jquery-1.11.1.min.js"></script>
        <script src="<?php echo $basePath; ?>js/autocomplete.min.js"></script>
        <script src="<?php echo $basePath; ?>js/datepicker.min.js"></script>
        <script src="<?php echo $basePath; ?>js/javascript.js"></script>
    </head>
    <body>
        <div class="wrapper">
            <div class="choose-page-header"> <!-- choose-page-header -->
                <span>login</span>
                <img src="<?php echo $basePath; ?>img/login.png">
                <img src="<?php echo $basePath; ?>img/map.png">
                <div class="clearfix"></div>
            </div>
            <div class="logo" style="margin: 10% auto 0px auto;"> <!-- logo -->
                <span id="first-letter">S</span><span id="other-letters">afar</span>
            </div>
            <div class="choose-page-search"> <!-- choose-page-search -->
                <label id="from-pos">From : </label>
                <label id="to-pos">To : </label>
                <form method="post" action="result.php">
                    <input class="autocomplete" id="from-city" type="text" name="from-city" value="Mashhad, Iran"> <!-- *** value must set by city location dynamically *** -->
                    <input class="autocomplete" id="to-city" type="text" name="to-city" placeholder="Enter a country, city">
                    <div><span>Go</span></div>
                </form>
                <img src="<?php echo $basePath; ?>img/choose-page-tour-icon.png">
                <img src="<?php echo $basePath; ?>img/choose-page-hotel-icon.png">
                <img src="<?php echo $basePath; ?>img/choose-page-plane-icon.png">
            </div>
            <div class="choose-page-footer"> <!-- choose-page-footer -->
                <a href="#"><img src="<?php echo $basePath; ?>img/twitter-icon.png"></a>
                <a href="#"><img src="<?php echo $basePath; ?>img/linkedin-icon.png"></a>
                <a href="#"><img src="<?php echo $basePath; ?>img/googleplus-icon.png"></a>
                <a href="#"><img src="<?php echo $basePath; ?>img/facebook-icon.png"></a>
                <ul>
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Privacy</a></li>
                    <li><a href="#">Terms</a></li>
                    <li><a href="#">Login</a></li>
                </ul>
            </div>
        </div>
    </body>
</html>
