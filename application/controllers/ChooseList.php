<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ChooseList extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url_helper');
        $this->load->helper('utility_helper');
        $this->load->library('form_validation');
        $this->load->helper('utility_helper');
        $this->load->model('city_model');
        $this->load->model('flight_model');
        $this->load->library('session');
        $this->load->database();
    }

    public function index() {
        $data = $this->flight_model->prepareData();
       
        $rules = $this->flight_model->setRules();
        $this->form_validation->set_data($data['form_data']);
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE) {
            redirect('/');
        } else {
            $this->load->view("choose_list/header", $data);
            $this->load->view("choose_list/my_list", $data);
            $this->load->view("choose_list/search_form", $data);
            $this->load->view("choose_list/result", $data);
            $this->load->view("choose_list/footer");
        }
    }

}
