<div class="price-page-trip-price"> <!-- trip-price -->
    <header><img src="<?= asset_url(); ?>img/price-icon.png"> Your Trip Price</header>
    <table>
        <tr>
            <td rowspan="2" style="width: 130px; font-size: 14px; color: #009cff; font-weight: 600; padding-left: 20px; vertical-align: top;">Fare Details</td>
            <td style="width: 425px;"><span style="font-weight: 600; padding-right: 10px; letter-spacing: .04em;">Child :</span> <?= $childrenNumber ?> x <?= $childrenPrice ?>=<span style="color: #1fbc00"><?= $childrenSumPrice ?>USD</span></td>
            <td style="width: 425px;"><span style="font-weight: 600; padding-right: 10px;">Adult(18-64) :</span> <?= $adultNumber ?> x <?= $adultPrice ?>=<span style="color: #1fbc00"><?= $adultSumPrice ?>USD</span></td>
        </tr>
        <tr>
            <td style="width: 425px;"><span style="font-weight: 600; padding-right: 15px;">Taxes :</span><span style="color: #1fbc00">656543USD</span></td>
            <td style="width: 425px;"><span style="font-weight: 600; padding-right: 10px;">Carrier-Imposed Fees :</span><span style="color: #1fbc00">548542USD</span></td>
        </tr>
    </table>
    <table style="border-spacing: 2px;">
        <tr>
            <td style="width: 250px; padding: 10px; font-size: 14px; font-weight: 600; padding-left: 20px; vertical-align: top;">
                <img src="<?= asset_url(); ?>img/square-icon.png"><a href="#"> Bagging and Optional Charges</a>
            </td>
            <td style="width: 350px; padding: 10px; font-size: 14px; font-weight: 600; padding-left: 20px; vertical-align: top;">
                <img src="<?= asset_url(); ?>img/square-icon.png"><a href="#"> Price and Tax Information</a>
            </td>
            <td style="width: 380px; padding: 10px; height: 10px; font-size: 16px; font-weight: 600; padding-left: 20px; vertical-align: top; background-color: #009cff;">
                <span style="color: #fff; padding-right: 20px;">Flight Subtotal :</span>
                <span style="color: #fffd36"><?= $totalPrice ?>USD</span>
            </td>
        </tr>
    </table>
</div>