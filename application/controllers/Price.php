<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Price extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url_helper');
        $this->load->helper('utility_helper');
        $this->load->model('city_model');
        $this->load->model('price_model');
        $this->load->library('form_validation');
        $this->load->library('MY_Form_validation');
        $this->load->helper('form');
        $this->load->library('restWebservice');
        $this->load->library('session');
        date_default_timezone_set("Asia/Tehran");
    }

    public function index() {
        $data = $this->price_model->getFlightData();
        $this->load->view('price/header');
        $this->load->view('price/trip_price', $data);
        $this->load->view('price/flight_details', $data);
        $this->load->view('price/passenger_details', $data);
        $this->load->view('price/page_caution');
        $this->load->view('price/footer');
    }

    public function savePassengerInfo() {
        $info = $this->input->post();
        $rules = $this->price_model->setRules();
        $this->form_validation->set_data($info);
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE) {
            $this->backToForm();
        } else {
            // send information to webservice
            $result = $this->price_model->sendPassengerInfoToWebservice($info);
            // if result is failed set flash message and call back to form
            if ($result['result'] == 'failed') {
                $errors = '';
                foreach ($result['errors'] as $err) {
                    $errors.=$err['message'] . '<br/>';
                }
                $this->session->set_flashdata('errors', $errors);
                $this->backToForm();
            }
            //else got to bank
            else {
                // save information to database 
                $ids = $this->price_model->sendPassengerInfoToDb($info, $result['refrenceId']);

                // $this->Price_model->insertRefrence($ids, $result['refrenceId']);
                $result = $this->price_model->confirmBook($result['refrenceId']);

                if ($result['result'] == 'success') {
                    // insert ticket number to data base
                    $this->price_model->insertTickets($ids, $result['tickets']);
                } else {
                    foreach ($result['errors'] as $err) {
                        $errors.=$err['message'] . '<br/>';
                    }
                    $this->session->set_flashdata('errors', $errors);
                    $this->backToForm();
                }
            }
        }
    }

    public function backToForm() {
        $data = $this->price_model->getFlightData();
        $this->load->view('price/header');
        $this->load->view('price/trip_price', $data);
        $this->load->view('price/flight_details', $data);
        $this->load->view('price/passenger_details', $data);
        $this->load->view('price/page_caution');
        $this->load->view('price/footer');
    }

}
