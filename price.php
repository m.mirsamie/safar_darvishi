<?php
$basePath = "";
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Safar</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="<?php echo $basePath; ?>css/autocomplete.min.css" rel="stylesheet">
        <link href="<?php echo $basePath; ?>css/datepicker.min.css" rel="stylesheet">
        <link href="<?php echo $basePath; ?>css/styles.css" rel="stylesheet">
        <script src="<?php echo $basePath; ?>js/jquery-1.11.1.min.js"></script>
        <script src="<?php echo $basePath; ?>js/autocomplete.min.js"></script>
        <script src="<?php echo $basePath; ?>js/datepicker.min.js"></script>
        <script src="<?php echo $basePath; ?>js/javascript.js"></script>
    </head>
    <body>
        <div class="wrapper">
            <div class="result-page-header"> <!-- price-page-header -->
                <div class="logo" style="float: left;"> <!-- logo -->
                    <span id="first-letter">S</span><span id="other-letters">afar</span>
                </div>
                <span>login</span>
                <img src="img/login.png">
                <div class="clearfix"></div>
            </div>
            <div class="timeline"> <!-- timeline -->
                <span class="inc-opacity" style="top: -12px; left: 50px;">Choose list</span>
                <span class="inc-opacity" style="top: -12px; left: 335px;">Price</span>
                <span style="top: -12px; left: 565px;">Passenger Details</span>
                <span style="top: -12px; left: 860px;">Payment</span>
                <img class="inc-opacity" src="img/timeline-choose.png">
                <img class="inc-opacity" src="<?php echo $basePath; ?>img/timeline-price.png">
                <img src="<?php echo $basePath; ?>img/timeline-passenger.png">
                <img src="<?php echo $basePath; ?>img/timeline-payment.png">
                <img src="<?php echo $basePath; ?>img/timeline-end.png">
                <div class="clearfix"></div>
            </div>
            <div class="price-page-trip-price"> <!-- trip-price -->
                <header><img src="<?php echo $basePath; ?>img/price-icon.png"> Your Trip Price</header>
                <table>
                    <tr>
                        <td rowspan="2" style="width: 130px; font-size: 14px; color: #009cff; font-weight: 600; padding-left: 20px; vertical-align: top;">Fare Details</td>
                        <td style="width: 425px;"><span style="font-weight: 600; padding-right: 10px; letter-spacing: .04em;">Child :</span> 2 x 111111=<span style="color: #1fbc00">222222USD</span></td>
                        <td style="width: 425px;"><span style="font-weight: 600; padding-right: 10px;">Adult(18-64) :</span> 2 x 111111=<span style="color: #1fbc00">222222USD</span></td>
                    </tr>
                    <tr>
                        <td style="width: 425px;"><span style="font-weight: 600; padding-right: 15px;">Taxes :</span><span style="color: #1fbc00">656543USD</span></td>
                        <td style="width: 425px;"><span style="font-weight: 600; padding-right: 10px;">Carrier-Imposed Fees :</span><span style="color: #1fbc00">548542USD</span></td>
                    </tr>
                </table>
                <table style="border-spacing: 2px;">
                    <tr>
                        <td style="width: 250px; padding: 10px; font-size: 14px; font-weight: 600; padding-left: 20px; vertical-align: top;">
                            <img src="<?php echo $basePath; ?>img/square-icon.png"><a href="#"> Bagging and Optional Charges</a>
                        </td>
                        <td style="width: 350px; padding: 10px; font-size: 14px; font-weight: 600; padding-left: 20px; vertical-align: top;">
                            <img src="<?php echo $basePath; ?>img/square-icon.png"><a href="#"> Price and Tax Information</a>
                        </td>
                        <td style="width: 380px; padding: 10px; height: 10px; font-size: 16px; font-weight: 600; padding-left: 20px; vertical-align: top; background-color: #009cff;">
                            <span style="color: #fff; padding-right: 20px;">Flight Subtotal :</span>
                            <span style="color: #fffd36">33972870USD</span>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="price-page-flight-detail"> <!-- flight-detail -->
                <header><img src="<?php echo $basePath; ?>img/flight-detail-icon.png"> Your Flight Details</header>
                <div style="border: 1px solid #1fbc00"> <!-- go-flight -->
                    <div class="price-page-flight-detail-sub-header">
                        <img src="<?php echo $basePath; ?>img/go-icon.png">
                        <span style="color: #009cff; padding-right: 25px;">Fri,Nov.20-2015</span>
                        <span style="color: #009cff">San Francisco, CA, US(SFO) to</span>
                        <span style="color: #1fbc00">Las Vegas, NV, US(LAS)</span>
                    </div>
                    <table>
                        <thead>
                            <tr>
                                <td>Depart :</td>
                                <td>Arrive :</td>
                                <td>Travel Time :</td>
                                <td><b>Flight Distance :</b></td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><span style="color: #009cff">10:57 pm</span></td>
                                <td><span style="color: #009cff">12:33 am +1 Day</span></td>
                                <td>1 hr and 36 min</td>
                                <td>414 miles</td>
                            </tr>
                            <tr>
                                <td><span style="color: #009cff">Fri, Nov.20-2015</span></td>
                                <td><span style="color: #009cff">Sat, Nov.20-2015</span></td>
                            </tr>
                            <tr>
                                <td>San Francisco, CA, US(SFO)</td>
                                <td>Las Vegas, NV, US(LAS)</td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="price-page-flight-info">
                        <span>Flight : </span>
                        UA455
                        <br>
                        <span>Aircraft : </span>
                        Boeing 737-900
                        <br>
                        <span>Fare Class : </span>
                        United Economy (I)
                        <br>
                        <span>Meal : </span>
                        None
                        <br>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div style="border: 1px solid #1fbc00; margin-top: 10px;"> <!-- return-flight -->
                    <div class="price-page-flight-detail-sub-header">
                        <img src="<?php echo $basePath; ?>img/return-icon.png">
                        <span style="color: #009cff; padding-right: 25px;">Tue,Nov.24-2015</span>
                        <span style="color: #009cff">Las Vegas, NV, US(LAS) to</span>
                        <span style="color: #1fbc00">San Francisco, CA, US(SFO)</span>
                    </div>
                    <table>
                        <thead>
                            <tr>
                                <td>Depart :</td>
                                <td>Arrive :</td>
                                <td>Travel Time :</td>
                                <td><b>Flight Distance :</b></td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><span style="color: #009cff">10:57 pm</span></td>
                                <td><span style="color: #009cff">12:33 am +1 Day</span></td>
                                <td>1 hr and 36 min</td>
                                <td>414 miles</td>
                            </tr>
                            <tr>
                                <td><span style="color: #009cff">Fri, Nov.20-2015</span></td>
                                <td><span style="color: #009cff">Sat, Nov.20-2015</span></td>
                            </tr>
                            <tr>
                                <td>San Francisco, CA, US(SFO)</td>
                                <td>Las Vegas, NV, US(LAS)</td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="price-page-flight-info">
                        <span>Flight : </span>
                        UA455
                        <br>
                        <span>Aircraft : </span>
                        Boeing 737-900
                        <br>
                        <span>Fare Class : </span>
                        United Economy (I)
                        <br>
                        <span>Meal : </span>
                        None
                        <br>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div style="border: 1px solid #7bbeff; margin-top: 10px;">
                <div class="price-page-passenger-detail"> <!-- passenger-detail -->
                    <header><img src="<?php echo $basePath; ?>img/passenger-detail-icon.png"> Passenger Details</header>
                    <div class="price-page-passenger-detail-sub-header">
                        <img src="<?php echo $basePath; ?>img/attention-icon.png"> 
                        Please enter all passenger's info as they appear on the passenger's passport or government-issued photo identification.
                    </div>
                    <form class="passenger-form">
                        <header>Passenger 1</header>
                        <div class="form-ctrl">
                            <div class="form-section" style="padding-left: 9px;">
                                <label>First name:* </label>
                                <input type="text">
                            </div>
                            <div class="form-section" style="padding-left: 15px;">
                                <label>Sur name:* </label>
                                <input type="text">
                            </div>
                            <div class="form-section" style="padding-left: 27px;">
                                <label>B-Date:* </label>
                                <input type="text" class="datepicker">
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-ctrl">
                            <div class="form-section" style="padding-left: 6px;">
                                <label>Nationality:* </label>
                                <select class="set-nat">
                                    <option value="1">Iran</option>
                                    <option value="2">The World</option>
                                </select>
                            </div>
                            <div class="form-section" style="padding-left: 17px;">
                                <label>Public ID:* </label>
                                <input type="text" class="pub-id">
                            </div>
                            <div class="form-section" style="padding-left: 24px;">
                                <label>Gender:* </label>
                                <select style="width: 100px;">
                                    <option>Male</option>
                                    <option>Female</option>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-ctrl nat-info">
                            <div class="form-section" style="padding-left: 2px;">
                                <label>Passport ID:* </label>
                                <input type="text">
                            </div>
                            <div class="form-section" style="padding-left: 2px;">
                                <label>P-Issued ID:* </label>
                                <input type="text">
                            </div>
                            <div class="form-section" style="padding-left: 2px;">
                                <label>P-Exp Date:* </label>
                                <input type="text" class="datepicker">
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <img src="img/special-assist-icon.png">
                        <div class="clearfix"></div>
                        <div class="passenger-detail-footer">
                            <div class="form-ctrl">
                                <div class="form-section" style="padding-left: 23px;">
                                    <label>Mobile:*</label>
                                    <input type="text">
                                </div>
                                <div class="form-section" style="padding-left: 14px;">
                                    <label>E-mail:*</label>
                                    <input type="text">
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="price-page-caution"> <!-- caution -->
                <header>Use caution when packing</header>
                <p>
                    Some everyday products, like electronic cigarettes and aerosol spray starch, can be dangerous when transported on the aircraft in carry-on and/or checked baggage. Changes in temperature or pressure can cause some items to leak, generate toxic fumes or start a fire. Carriage of prohibited items may result in fines or in certain cases imprisonment. Please ensure there are no forbidden hazardous materials in your baggage like:
                </p>
                <ul>
                    <li><img src="img/cation1.png"></li>
                    <li><img src="img/cation2.png"></li>
                    <li><img src="img/cation3.png"></li>
                    <li><img src="img/cation4.png"></li>
                    <li><img src="img/cation5.png"></li>
                    <li><img src="img/cation6.png"></li>
                    <li><img src="img/cation7.png"></li>
                </ul>
            </div>
            <div class="price-page-footer">  <!-- price-page-footer --> 
                <div class="result-page-box-submit">
                    <img class="result-page-dec-opacity" src="img/page-backward-icon.png">
                    <img src="img/page2-icon.png">
                    <img src="img/page-forward-icon.png">
                </div>
                <a href="#"><img src="<?php echo $basePath; ?>img/twitter-icon.png"></a>
                <a href="#"><img src="<?php echo $basePath; ?>img/linkedin-icon.png"></a>
                <a href="#"><img src="<?php echo $basePath; ?>img/googleplus-icon.png"></a>
                <a href="#"><img src="<?php echo $basePath; ?>img/facebook-icon.png"></a>
                <ul>
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Privacy</a></li>
                    <li><a href="#">Terms</a></li>
                    <li><a href="#">Login</a></li>
                </ul>
            </div>
        </div>
    </body>
</html>
