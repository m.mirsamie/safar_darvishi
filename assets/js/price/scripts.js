$(document).ready(function() {
    //passenger-passport-appear

    $('.nat-info').hide();
    $('.set-nat').change(function() {
        var id = $(this).attr('id');
        var selected_option = $(this).val();
        if (selected_option==='25') {
            $('#nat-info' + id + '').hide();
            $('#pub-id'+id+'').prop('disabled', false);
           
        } else {
            $('#nat-info' + id + '').show();
            $('#pub-id'+id+'').prop('disabled', true);
        }
    });
    $('#safar-price-save-info').click(function() {
        $('.passenger-form').submit();

    });
    $(".datepicker").datepicker({
         dateFormat: 'dd/mm/yy'
    });
});