<?php
$basePath = "";
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Safar</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="<?php echo $basePath; ?>css/autocomplete.min.css" rel="stylesheet">
        <link href="<?php echo $basePath; ?>css/datepicker.min.css" rel="stylesheet">
        <link href="<?php echo $basePath; ?>css/styles.css" rel="stylesheet">
        <script src="<?php echo $basePath; ?>js/jquery-1.11.1.min.js"></script>
        <script src="<?php echo $basePath; ?>js/autocomplete.min.js"></script>
        <script src="<?php echo $basePath; ?>js/datepicker.min.js"></script>
        <script src="<?php echo $basePath; ?>js/javascript.js"></script>
    </head>
    <body>
        <div class="wrapper">
            <div class="result-page-header"> <!-- result-page-header -->
                <div class="logo" style="float: left;"> <!-- logo -->
                    <span id="first-letter">S</span><span id="other-letters">afar</span>
                </div>
                <span>login</span>
                <img src="img/login.png">
                <div class="clearfix"></div>
            </div>
            <div class="result-page-search"> <!-- result-page-search -->
                <form>
                    <label>From : </label><input class="autocomplete"type="text" placeholder="Enter a country, city" value="Mashhad, Iran">
                    <label style="margin-left: 25px;">To : </label><input class="autocomplete" type="text" placeholder="Enter a country, city" value="Tehran">
                    <input style="margin-left: 40px;" type="text" class="datepicker" value="11/19/2015">
                    <input type="text" class="datepicker" placeholder="12/02/2015">
                </form>
            </div>
            <div class="timeline"> <!-- timeline -->
                <span class="result-page-set-opacity" style="top: -12px; left: 50px;">Choose list</span>
                <span style="top: -12px; left: 335px;">Price</span>
                <span style="top: -12px; left: 565px;">Passenger Details</span>
                <span style="top: -12px; left: 860px;">Payment</span>
                <img class="result-page-set-opacity" src="img/timeline-choose.png">
                <img src="<?php echo $basePath; ?>img/timeline-price.png">
                <img src="<?php echo $basePath; ?>img/timeline-passenger.png">
                <img src="<?php echo $basePath; ?>img/timeline-payment.png">
                <img src="<?php echo $basePath; ?>img/timeline-end.png">
                <div class="clearfix"></div>
            </div>
            <div class="result-page-box"> <!-- results -->
                <div class="result-page-box-content">
                    <header><img src="<?php echo $basePath; ?>img/plane-icon.png">Flights List</header>
                    <ul>
                        <li>
                            Price
                            <span>
                                <form method="post">
                                    <div class="form-vertical">
                                        <button type="submit">Ascending Price</button>
                                    </div>
                                    <div class="form-vertical">
                                        <button type="submit">Descending Price</button>
                                    </div>
                                </form>
                            </span>
                        </li>
                        <li>
                            Airlines
                            <span>
                                <form method="post">
                                    <div class="form-vertical">
                                        <button type="submit">Meraj</button>
                                    </div>
                                    <div class="form-vertical">
                                        <button type="submit">Aseman</button>
                                    </div>
                                    <div class="form-vertical">
                                        <button type="submit">Mahan</button>
                                    </div>
                                    <div class="form-vertical">
                                        <button type="submit">Homa</button>
                                    </div>
                                </form>
                            </span>
                        </li>
                        <li>
                            Stops
                            <span>
                                <form method="post">
                                    <div class="form-vertical">
                                        <button type="submit">Roundtrip</button>
                                    </div>
                                    <div class="form-vertical">
                                        <button type="submit">One way</button>
                                    </div>
                                    <div class="form-vertical">
                                        <button type="submit">Multiple destinations</button>
                                    </div>
                                </form>
                            </span>
                        </li>
                    </ul>
                    <div class="scrollbar" id="scroll-style">
                        <div class="result-page-result">
                            <span style="width: 70px; height: 80px; padding-top: 20px;">
                                From
                                <br>
                                <span style="width: 70px; color: #1fbc00; font-weight: 600;">250$</span>
                                <br>
                            </span>
                            <span style="width: 100px; height: 80px; padding-top: 25px;">
                                <img src="<?php echo $basePath; ?>img/meraj-logo.png">
                            </span>
                            <span style="width: 100px; height: 80px; padding-top: 30px;">10-15-2015</span>
                            <span style="width: 30px; height: 80px; padding-top: 30px;">
                                <img class="result-btn" src="<?php echo $basePath; ?>img/plus-icon.png" data-swap="<?php echo $basePath; ?>img/minus-icon.png">
                            </span>
                            <div class="clearfix"></div>
                            <div class="result-page-result-detail result-page-toggle">
                                <span style="width: 70px; height: 80px; padding-top: 20px;">
                                    8:05 am
                                    <br>
                                    9:15 am
                                    <br>
                                </span>
                                <span style="width: 100px; height: 80px; padding-top: 25px;">
                                    <img src="<?php echo $basePath; ?>img/meraj-logo.png">
                                </span>
                                <span style="width: 80px; height: 80px; padding-top: 30px;">1h 10m</span>
                                <span style="width: 50px; height: 80px; padding-top: 20px;">
                                    Non
                                    <br>
                                    Stop
                                    <br>
                                </span>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="result-page-result">
                            <span style="width: 70px; height: 80px; padding-top: 20px;">
                                From
                                <br>
                                <span style="width: 70px; color: #1fbc00; font-weight: 600;">250$</span>
                                <br>
                            </span>
                            <span style="width: 100px; height: 80px; padding-top: 25px;">
                                <img src="<?php echo $basePath; ?>img/meraj-logo.png">
                            </span>
                            <span style="width: 100px; height: 80px; padding-top: 30px;">10-15-2015</span>
                            <span style="width: 30px; height: 80px; padding-top: 30px;">
                                <img class="result-btn" src="<?php echo $basePath; ?>img/plus-icon.png" data-swap="<?php echo $basePath; ?>img/minus-icon.png">
                            </span>
                            <div class="clearfix"></div>
                            <div class="result-page-result-detail result-page-toggle">
                                <span style="width: 70px; height: 80px; padding-top: 20px;">
                                    8:05 am
                                    <br>
                                    9:15 am
                                    <br>
                                </span>
                                <span style="width: 100px; height: 80px; padding-top: 25px;">
                                    <img src="<?php echo $basePath; ?>img/meraj-logo.png">
                                </span>
                                <span style="width: 80px; height: 80px; padding-top: 30px;">1h 10m</span>
                                <span style="width: 50px; height: 80px; padding-top: 20px;">
                                    Non
                                    <br>
                                    Stop
                                    <br>
                                </span>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="result-page-result">
                            <span style="width: 70px; height: 80px; padding-top: 20px;">
                                From
                                <br>
                                <span style="width: 70px; color: #1fbc00; font-weight: 600;">250$</span>
                                <br>
                            </span>
                            <span style="width: 100px; height: 80px; padding-top: 25px;">
                                <img src="<?php echo $basePath; ?>img/meraj-logo.png">
                            </span>
                            <span style="width: 100px; height: 80px; padding-top: 30px;">10-15-2015</span>
                            <span style="width: 30px; height: 80px; padding-top: 30px;">
                                <img class="result-btn" src="<?php echo $basePath; ?>img/plus-icon.png" data-swap="<?php echo $basePath; ?>img/minus-icon.png">
                            </span>
                            <div class="clearfix"></div>
                            <div class="result-page-result-detail result-page-toggle">
                                <span style="width: 70px; height: 80px; padding-top: 20px;">
                                    8:05 am
                                    <br>
                                    9:15 am
                                    <br>
                                </span>
                                <span style="width: 100px; height: 80px; padding-top: 25px;">
                                    <img src="<?php echo $basePath; ?>img/meraj-logo.png">
                                </span>
                                <span style="width: 80px; height: 80px; padding-top: 30px;">1h 10m</span>
                                <span style="width: 50px; height: 80px; padding-top: 20px;">
                                    Non
                                    <br>
                                    Stop
                                    <br>
                                </span>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="result-page-result">
                            <span style="width: 70px; height: 80px; padding-top: 20px;">
                                From
                                <br>
                                <span style="width: 70px; color: #1fbc00; font-weight: 600;">250$</span>
                                <br>
                            </span>
                            <span style="width: 100px; height: 80px; padding-top: 25px;">
                                <img src="<?php echo $basePath; ?>img/meraj-logo.png">
                            </span>
                            <span style="width: 100px; height: 80px; padding-top: 30px;">10-15-2015</span>
                            <span style="width: 30px; height: 80px; padding-top: 30px;">
                                <img class="result-btn" src="<?php echo $basePath; ?>img/plus-icon.png" data-swap="<?php echo $basePath; ?>img/minus-icon.png">
                            </span>
                            <div class="clearfix"></div>
                            <div class="result-page-result-detail result-page-toggle">
                                <span style="width: 70px; height: 80px; padding-top: 20px;">
                                    8:05 am
                                    <br>
                                    9:15 am
                                    <br>
                                </span>
                                <span style="width: 100px; height: 80px; padding-top: 25px;">
                                    <img src="<?php echo $basePath; ?>img/meraj-logo.png">
                                </span>
                                <span style="width: 80px; height: 80px; padding-top: 30px;">1h 10m</span>
                                <span style="width: 50px; height: 80px; padding-top: 20px;">
                                    Non
                                    <br>
                                    Stop
                                    <br>
                                </span>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="result-page-result">
                            <span style="width: 70px; height: 80px; padding-top: 20px;">
                                From
                                <br>
                                <span style="width: 70px; color: #1fbc00; font-weight: 600;">250$</span>
                                <br>
                            </span>
                            <span style="width: 100px; height: 80px; padding-top: 25px;">
                                <img src="<?php echo $basePath; ?>img/meraj-logo.png">
                            </span>
                            <span style="width: 100px; height: 80px; padding-top: 30px;">10-15-2015</span>
                            <span style="width: 30px; height: 80px; padding-top: 30px;">
                                <img class="result-btn" src="<?php echo $basePath; ?>img/plus-icon.png" data-swap="<?php echo $basePath; ?>img/minus-icon.png">
                            </span>
                            <div class="clearfix"></div>
                            <div class="result-page-result-detail result-page-toggle">
                                <span style="width: 70px; height: 80px; padding-top: 20px;">
                                    8:05 am
                                    <br>
                                    9:15 am
                                    <br>
                                </span>
                                <span style="width: 100px; height: 80px; padding-top: 25px;">
                                    <img src="<?php echo $basePath; ?>img/meraj-logo.png">
                                </span>
                                <span style="width: 80px; height: 80px; padding-top: 30px;">1h 10m</span>
                                <span style="width: 50px; height: 80px; padding-top: 25px;">
                                    Non
                                    <br>
                                    Stop
                                    <br>
                                </span>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="result-page-box-content" style="margin: 0 25px;">
                    <header><img src="<?php echo $basePath; ?>img/hotel-icon.png">Hotels List</header>
                    <ul>
                        <li>
                            Stars
                            <span>
                                <form method="post">
                                    <div class="form-vertical">
                                        <button type="submit">One star</button>
                                    </div>
                                    <div class="form-vertical">
                                        <button type="submit">Two stars</button>
                                    </div>
                                    <div class="form-vertical">
                                        <button type="submit">Three stars</button>
                                    </div>
                                    <div class="form-vertical">
                                        <button type="submit">Four stars</button>
                                    </div>
                                    <div class="form-vertical">
                                        <button type="submit">Five stars</button>
                                    </div>
                                </form>
                            </span>
                        </li>
                        <li>
                            Names
                            <span>
                                <form method="post">
                                    <div class="form-vertical">
                                        <button type="submit">Darvishi Hotels</button>
                                    </div>
                                    <div class="form-vertical">
                                        <button type="submit">Azadi Hotels</button>
                                    </div>
                                    <div class="form-vertical">
                                        <button type="submit">Homa Hotels</button>
                                    </div>
                                </form>
                            </span>
                        </li>
                    </ul>
                    <div class="scrollbar" id="scroll-style">
                        <div class="result-page-result">
                            <span style="width: 70px; height: 80px; padding-top: 25px;">
                                <img src="<?php echo $basePath; ?>img/4stars-icon.png">
                            </span>
                            <span style="width: 100px; height: 80px; padding-top: 18px;">
                                <img src="<?php echo $basePath; ?>img/darvishi-logo.png">
                            </span>
                            <span style="width: 100px; height: 80px; padding-top: 30px;">10-15-2015</span>
                            <span style="width: 30px; height: 80px; padding-top: 30px;"><img class="result-btn" src="<?php echo $basePath; ?>img/plus-icon.png" data-swap="<?php echo $basePath; ?>img/minus-icon.png"></span>
                            <div class="clearfix"></div>
                            <div class="result-page-result-detail result-page-toggle">
                                <span style="width: 70px; height: 80px; padding-top: 30px; color: #1fbc00; font-weight: 600;">
                                    1000$
                                </span>
                                <span style="width: 230px; height: 80px; padding-top: 30px;">
                                    Penthouse Rooms
                                </span>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="result-page-result">
                            <span style="width: 70px; height: 80px; padding-top: 25px;">
                                <img src="<?php echo $basePath; ?>img/4stars-icon.png">
                            </span>
                            <span style="width: 100px; height: 80px; padding-top: 18px;">
                                <img src="<?php echo $basePath; ?>img/darvishi-logo.png">
                            </span>
                            <span style="width: 100px; height: 80px; padding-top: 30px;">10-15-2015</span>
                            <span style="width: 30px; height: 80px; padding-top: 30px;"><img class="result-btn" src="<?php echo $basePath; ?>img/plus-icon.png" data-swap="<?php echo $basePath; ?>img/minus-icon.png"></span>
                            <div class="clearfix"></div>
                            <div class="result-page-result-detail result-page-toggle">
                                <span style="width: 70px; height: 80px; padding-top: 30px; color: #1fbc00; font-weight: 600;">
                                    1000$
                                </span>
                                <span style="width: 230px; height: 80px; padding-top: 30px;">
                                    Penthouse Rooms
                                </span>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="result-page-result">
                            <span style="width: 70px; height: 80px; padding-top: 25px;">
                                <img src="<?php echo $basePath; ?>img/4stars-icon.png">
                            </span>
                            <span style="width: 100px; height: 80px; padding-top: 18px;">
                                <img src="<?php echo $basePath; ?>img/darvishi-logo.png">
                            </span>
                            <span style="width: 100px; height: 80px; padding-top: 30px;">10-15-2015</span>
                            <span style="width: 30px; height: 80px; padding-top: 30px;"><img class="result-btn" src="<?php echo $basePath; ?>img/plus-icon.png" data-swap="<?php echo $basePath; ?>img/minus-icon.png"></span>
                            <div class="clearfix"></div>
                            <div class="result-page-result-detail result-page-toggle">
                                <span style="width: 70px; height: 80px; padding-top: 30px; color: #1fbc00; font-weight: 600;">
                                    1000$
                                </span>
                                <span style="width: 230px; height: 80px; padding-top: 30px;">
                                    Penthouse Rooms
                                </span>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="result-page-result">
                            <span style="width: 70px; height: 80px; padding-top: 25px;">
                                <img src="<?php echo $basePath; ?>img/4stars-icon.png">
                            </span>
                            <span style="width: 100px; height: 80px; padding-top: 18px;">
                                <img src="<?php echo $basePath; ?>img/darvishi-logo.png">
                            </span>
                            <span style="width: 100px; height: 80px; padding-top: 30px;">10-15-2015</span>
                            <span style="width: 30px; height: 80px; padding-top: 30px;"><img class="result-btn" src="<?php echo $basePath; ?>img/plus-icon.png" data-swap="<?php echo $basePath; ?>img/minus-icon.png"></span>
                            <div class="clearfix"></div>
                            <div class="result-page-result-detail result-page-toggle">
                                <span style="width: 70px; height: 80px; padding-top: 30px; color: #1fbc00; font-weight: 600;">
                                    1000$
                                </span>
                                <span style="width: 230px; height: 80px; padding-top: 30px;">
                                    Penthouse Rooms
                                </span>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="result-page-result">
                            <span style="width: 70px; height: 80px; padding-top: 25px;">
                                <img src="<?php echo $basePath; ?>img/4stars-icon.png">
                            </span>
                            <span style="width: 100px; height: 80px; padding-top: 18px;">
                                <img src="<?php echo $basePath; ?>img/darvishi-logo.png">
                            </span>
                            <span style="width: 100px; height: 80px; padding-top: 30px;">10-15-2015</span>
                            <span style="width: 30px; height: 80px; padding-top: 30px;"><img class="result-btn" src="<?php echo $basePath; ?>img/plus-icon.png" data-swap="<?php echo $basePath; ?>img/minus-icon.png"></span>
                            <div class="clearfix"></div>
                            <div class="result-page-result-detail result-page-toggle">
                                <span style="width: 70px; height: 80px; padding-top: 30px; color: #1fbc00; font-weight: 600;">
                                    1000$
                                </span>
                                <span style="width: 230px; height: 80px; padding-top: 30px;">
                                    Penthouse Rooms
                                </span>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="result-page-result">
                            <span style="width: 70px; height: 80px; padding-top: 25px;">
                                <img src="<?php echo $basePath; ?>img/4stars-icon.png">
                            </span>
                            <span style="width: 100px; height: 80px; padding-top: 18px;">
                                <img src="<?php echo $basePath; ?>img/darvishi-logo.png">
                            </span>
                            <span style="width: 100px; height: 80px; padding-top: 30px;">10-15-2015</span>
                            <span style="width: 30px; height: 80px; padding-top: 30px;"><img class="result-btn" src="<?php echo $basePath; ?>img/plus-icon.png" data-swap="<?php echo $basePath; ?>img/minus-icon.png"></span>
                            <div class="clearfix"></div>
                            <div class="result-page-result-detail result-page-toggle">
                                <span style="width: 70px; height: 80px; padding-top: 30px; color: #1fbc00; font-weight: 600;">
                                    1000$
                                </span>
                                <span style="width: 230px; height: 80px; padding-top: 30px;">
                                    Penthouse Rooms
                                </span>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="result-page-box-content">
                    <header><img src="<?php echo $basePath; ?>img/tour-icon.png">Tours List</header>
                    <ul>
                        <li>
                            Price
                            <span>
                                <form>
                                    <div class="form-vertical">
                                        <button>Ascending Price</button>
                                    </div>
                                    <div class="form-vertical">
                                        <button>Descending Price</button>
                                    </div>
                                </form>
                            </span>
                        </li>
                        <li>
                            Days/Nights
                            <span></span>
                        </li>
                    </ul>
                    <div class="scrollbar" id="scroll-style">
                        <div class="result-page-result">
                            <span style="width: 70px; height: 80px; padding-top: 20px;">
                                From
                                <br>
                                <span style="color: #7bbeff; font-weight: 600;">250$</span>
                                <br>
                            </span>
                            <span style="width: 100px; height: 80px; padding-top: 20px;">
                                3 Days
                                <br>
                                4 Nights
                                <br>
                            </span>
                            <span style="width: 100px; height: 80px; padding-top: 30px;">10-15-2015</span>
                            <span style="width: 30px; height: 80px; padding-top: 30px;"><img class="result-btn" src="<?php echo $basePath; ?>img/plus-icon.png" data-swap="<?php echo $basePath; ?>img/minus-icon.png"></span>
                            <div class="clearfix"></div>
                            <div class="result-page-result-detail result-page-toggle">
                                <span style="width: 150px; height: 80px; padding-top: 30px; color: #1fbc00; font-weight: 600;">
                                    Adibian Agency
                                </span>
                                <span style="width: 150px; height: 80px; padding-top: 30px; color: #7bbeff;">
                                     Ghasr Hotel
                                </span>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="result-page-result">
                            <span style="width: 70px; height: 80px; padding-top: 20px;">
                                From
                                <br>
                                <span style="color: #7bbeff; font-weight: 600;">250$</span>
                                <br>
                            </span>
                            <span style="width: 100px; height: 80px; padding-top: 20px;">
                                3 Days
                                <br>
                                4 Nights
                                <br>
                            </span>
                            <span style="width: 100px; height: 80px; padding-top: 30px;">10-15-2015</span>
                            <span style="width: 30px; height: 80px; padding-top: 30px;"><img class="result-btn" src="<?php echo $basePath; ?>img/plus-icon.png" data-swap="<?php echo $basePath; ?>img/minus-icon.png"></span>
                            <div class="clearfix"></div>
                            <div class="result-page-result-detail result-page-toggle">
                                <span style="width: 130px; height: 80px; padding-top: 30px; color: #1fbc00; font-weight: 600;">
                                    Adibian Agency
                                </span>
                                <span style="width: 170px; height: 80px; padding-top: 30px; color: #7bbeff;">
                                     Ghasr Hotel
                                </span>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="result-page-result">
                            <span style="width: 70px; height: 80px; padding-top: 20px;">
                                From
                                <br>
                                <span style="color: #7bbeff; font-weight: 600;">250$</span>
                                <br>
                            </span>
                            <span style="width: 100px; height: 80px; padding-top: 20px;">
                                3 Days
                                <br>
                                4 Nights
                                <br>
                            </span>
                            <span style="width: 100px; height: 80px; padding-top: 30px;">10-15-2015</span>
                            <span style="width: 30px; height: 80px; padding-top: 30px;"><img class="result-btn" src="<?php echo $basePath; ?>img/plus-icon.png" data-swap="<?php echo $basePath; ?>img/minus-icon.png"></span>
                            <div class="clearfix"></div>
                            <div class="result-page-result-detail result-page-toggle">
                                <span style="width: 170px; height: 80px; padding-top: 30px; color: #1fbc00; font-weight: 600;">
                                    Adibian Agency
                                </span>
                                <span style="width: 130px; height: 80px; padding-top: 30px; color: #7bbeff;">
                                     Ghasr Hotel
                                </span>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="result-page-result">
                            <span style="width: 70px; height: 80px; padding-top: 20px;">
                                From
                                <br>
                                <span style="color: #7bbeff; font-weight: 600;">250$</span>
                                <br>
                            </span>
                            <span style="width: 100px; height: 80px; padding-top: 20px;">
                                3 Days
                                <br>
                                4 Nights
                                <br>
                            </span>
                            <span style="width: 100px; height: 80px; padding-top: 30px;">10-15-2015</span>
                            <span style="width: 30px; height: 80px; padding-top: 30px;"><img class="result-btn" src="<?php echo $basePath; ?>img/plus-icon.png" data-swap="<?php echo $basePath; ?>img/minus-icon.png"></span>
                            <div class="clearfix"></div>
                            <div class="result-page-result-detail result-page-toggle">
                                <span style="width: 130px; height: 80px; padding-top: 30px; color: #1fbc00; font-weight: 600;">
                                    Adibian Agency
                                </span>
                                <span style="width: 170px; height: 80px; padding-top: 30px; color: #7bbeff;">
                                     Ghasr Hotel
                                </span>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="result-page-result">
                            <span style="width: 70px; height: 80px; padding-top: 20px;">
                                From
                                <br>
                                <span style="color: #7bbeff; font-weight: 600;">250$</span>
                                <br>
                            </span>
                            <span style="width: 100px; height: 80px; padding-top: 20px;">
                                3 Days
                                <br>
                                4 Nights
                                <br>
                            </span>
                            <span style="width: 100px; height: 80px; padding-top: 30px;">10-15-2015</span>
                            <span style="width: 30px; height: 80px; padding-top: 30px;"><img class="result-btn" src="<?php echo $basePath; ?>img/plus-icon.png" data-swap="<?php echo $basePath; ?>img/minus-icon.png"></span>
                            <div class="clearfix"></div>
                            <div class="result-page-result-detail result-page-toggle">
                                <span style="width: 130px; height: 80px; padding-top: 30px; color: #1fbc00; font-weight: 600;">
                                    Adibian Agency
                                </span>
                                <span style="width: 170px; height: 80px; padding-top: 30px; color: #7bbeff;">
                                     Ghasr Hotel
                                </span>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="result-page-result">
                            <span style="width: 70px; height: 80px; padding-top: 20px;">
                                From
                                <br>
                                <span style="color: #7bbeff; font-weight: 600;">250$</span>
                                <br>
                            </span>
                            <span style="width: 100px; height: 80px; padding-top: 20px;">
                                3 Days
                                <br>
                                4 Nights
                                <br>
                            </span>
                            <span style="width: 100px; height: 80px; padding-top: 30px;">10-15-2015</span>
                            <span style="width: 30px; height: 80px; padding-top: 30px;"><img class="result-btn" src="<?php echo $basePath; ?>img/plus-icon.png" data-swap="<?php echo $basePath; ?>img/minus-icon.png"></span>
                            <div class="clearfix"></div>
                            <div class="result-page-result-detail result-page-toggle">
                                <span style="width: 130px; height: 80px; padding-top: 30px; color: #1fbc00; font-weight: 600;">
                                    Adibian Agency
                                </span>
                                <span style="width: 170px; height: 80px; padding-top: 30px; color: #7bbeff;">
                                     Ghasr Hotel
                                </span>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="result-page-footer">  <!-- result-page-footer --> 
                <a href="#"><img src="<?php echo $basePath; ?>img/twitter-icon.png"></a>
                <a href="#"><img src="<?php echo $basePath; ?>img/linkedin-icon.png"></a>
                <a href="#"><img src="<?php echo $basePath; ?>img/googleplus-icon.png"></a>
                <a href="#"><img src="<?php echo $basePath; ?>img/facebook-icon.png"></a>
                <ul>
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Privacy</a></li>
                    <li><a href="#">Terms</a></li>
                    <li><a href="#">Login</a></li>
                </ul>
            </div>
        </div>
    </body>
</html>

