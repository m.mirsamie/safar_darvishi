<div class="timeline"> <!-- timeline -->
    <span class="inc-opacity" style="top: -12px; left: 50px;">Choose list</span>
    <span style="top: -12px; left: 335px;">Price</span>
    <span style="top: -12px; left: 565px;">Passenger Details</span>
    <span style="top: -12px; left: 860px;">Payment</span>
    <img class="inc-opacity" src="<?= asset_url() ?>img/timeline-choose.png">
    <img src="<?= asset_url() ?>img/timeline-price.png">
    <img src="<?= asset_url() ?>img/timeline-passenger.png">
    <img src="<?= asset_url() ?>img/timeline-payment.png">
    <img src="<?= asset_url() ?>img/timeline-end.png">
    <div class="clearfix"></div>
</div>
<div class="result-page-box"> <!-- results -->
    <div class="result-page-box-content" id="safar-flight-content">
        <?php $this->load->view('choose_list/flights_list'); ?>
    </div>
    <?php $this->load->view('choose_list/hotels_list'); ?>
    <?php $this->load->view('choose_list/tours_list'); ?>
    <div class="clearfix"></div>
</div>
