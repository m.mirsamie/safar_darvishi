$(document).ready(function() {

    $(".datepicker").datepicker({
        dateFormat: 'yy/mm/dd'
    });
    //alert("Your location is: " +geoplugin_city());

//    if (geoplugin_city() !== '')
//        $('#from-city').val(geoplugin_city() + ', ' + geoplugin_countryName());
//    else
//        $('#from-city').val(geoplugin_countryName());

    var int;
    var time = setInterval(function() {
        var newCity = cities[Math.floor(Math.random() * cities.length)];
        $('#to-city').attr('placeholder', newCity);
        if (int === 0)
            clearInterval(time);
    }, 3000);
    $('.autocomplete').autoComplete({
        minChars: 2,
        source: function(term, suggest) {
            term = term.toLowerCase();
            var choices = cities; //load all cities from DB here
            var suggestions = [];
            for (i = 0; i < choices.length; i++)
                if (~choices[i].toLowerCase().indexOf(term))
                    suggestions.push(choices[i]);
            suggest(suggestions);
        },
        onSelect: function(event, ui) {
            research();
        }
    });
    //result-filter
    $('.result-page-box ul li span').hide();
    $('.result-page-box ul li').mouseover(function() {
        $(this).find('span').stop().slideDown('slow');
    });
    $('.result-page-box ul li').mouseout(function() {
        $(this).find('span').stop().slideUp('slow');
    });

    //result-detail
    $('.result-page-toggle').hide();
    $('.result-btn').click(function() {
        $(this).parent().parent().find(".result-page-toggle").stop().toggle('slow');
    });

    //result-detail-btn
    $('.result-btn').click(function() {
        var _this = $(this);
        var current = _this.attr("src");
        var swap = _this.attr("data-swap");
        _this.attr('src', swap).attr("data-swap", current);
    });
    //mylist
    $('.result-page-mylist-flight').hide();
    $('.mylist-flight').click(function() {
        $(this).parent().parent().find('.result-page-mylist-flight').stop().toggle('slow');
    });
    //mylist - counter
    $('.inc').click(function() {
        var _txt = $(this).parent().find('.adult-counter');
        var value = _txt.val();
        value++;
        _txt.val(value);
    });

    $('.dec').unbind('click').bind('click', function() {
        var value = $('.adult-counter').val();
        value--;
        $('.adult-counter').val(value);
    });
    var refresh_js_after_ajax = function() {
        $('.result-page-toggle').hide();
        $('.result-btn').click(function() {
            $(this).parent().parent().find(".result-page-toggle").stop().toggle('slow');
        });
        //result-filter
        $('.result-page-box ul li span').hide();
        $('.result-page-box ul li').mouseover(function() {
            $(this).find('span').stop().slideDown('slow');
        });
        $('.result-page-box ul li').mouseout(function() {
            $(this).find('span').stop().slideUp('slow');
        });
    };
    var research = function() {
        if (confirm('Do You Want To Refresh Search Results?')) {
            var fromCity = $('#research-from-city').val();
            var toCity = $('#research-to-city').val();
            var fromDate = $('#research-from-date').val();
            var toDate = $('#research-to-date').val();
            $.ajax({
                type: 'POST',
                url: base_url + "ajax/research",
                data: {fromCity: fromCity, toCity: toCity, fromDate: fromDate, toDate: toDate}
            }).done(function(result) {
                //console.log(JSON.parse(result));
                //result = JSON.parse(result);
                if (result !== 'false') {
                    $('#safar-flight-content').html(result);   // load html with data from php
                    refresh_js_after_ajax();
                }
                else {
                    alert('Your Search Parameters Is Invalid.');
                }
            });
        }
    };
    var sort_flight = function(order) {
        var parent = $('#safar-flight');
        var childSelector = "div#safar-flight-result";
        var keySelector = "span#safar-flight-price";
        var items = parent.children(childSelector).sort(function(a, b) {
            var vA = $(keySelector, a).attr('data-value');
            var vB = $(keySelector, b).attr('data-value');
            if (order === 'desc')
                return parseInt(vB) - parseInt(vA);
            else
                return  parseInt(vA) - parseInt(vB);
        });
        parent.append(items);
    };
    var add_to_flight_list = function(item) {
        var html = "";
        html += "<div class='mylist-flight-content-oneway' id='safar-flight-mylist-content' data-arrival-airport=" + item.arrival_airport + " data-departure-airport=" + item.departure_airport + " data-flight-number=" + item.flight_number + " data-id=" + item.tflight + "><span style = 'width: 40px;' >";
        html += "<img style = 'float: left;' src = '" + asset_url + "img/mylist-plane-icon.png'></span>";
        html += "<span id='safar-flight-list-date' data-arrival-date=" + item.arrival_date + " data-departure-date=" + item.departure_date + " style = 'width: 130px; line-height: 40px;' > <span style='color: #009cff; width: 140px;' > " + item.departure_date + " </span></span >";
        html += "<span style = 'width: 100px; margin-top: 5px;' > <img src = '" + asset_url + "img/meraj-logo.png' > </span>";
        html += "<span id='safar-flight-list-time' data-departure-time=" + item.departure_time + " data-arrival-time=" + item.arrival_time + " style = 'width: 190px; line-height: 40px;' > " + item.departure_time + " - " + item.arrival_time + " </span>";
        html += "<span id='safar-flight-list-length' data-length-minute=" + item.length_minute + " data-length-hour=" + item.length_hour + " style = 'width: 70px; line-height: 40px;' > " + item.length_hour + "h " + item.length_minute + "m </span>";
        html += "<span style = 'width: 120px; line-height: 40px;' > <span style = 'color: #7bbeff; width: 120px;' > Airbus A319 </span></span >";
        html += "<span id='safar-flight-list-price' data-price=" + item.price + " style = 'width: 100px; line-height: 40px;' > " + item.price + "RI </span>";
        html += "<span style = 'width: 20px; line-height: 40px;' > <img class = 'mylist-res-close' src = '" + asset_url + "img/mylist-res-close.png' > </span>";
        html += "</div>";
        $('#safar-flight-mylist').append(html);
        var flight_number = parseInt($('#safar-flight-list-number').text()) + 1;
        $('#safar-flight-list-number').text(flight_number);
        $.ajax({
            type: 'POST',
            url: base_url + "ajax/saveFlightList",
            data: {data: JSON.stringify(item), type: 'add'}
        }).done(function(result) {
            console.log(result);
        });

    };
    var remove_from_flight_list = function(item) {
        var parent = $('#safar-flight-mylist');
        var childSelector = "div#safar-flight-mylist-content";
        parent.children(childSelector).each(function() {
            if ($(this).data('id') === item['tflight']) {
                $(this).remove();
                var flight_number = parseInt($('#safar-flight-list-number').text()) - 1;
                $('#safar-flight-list-number').text(flight_number);
                $.ajax({
                    type: 'POST',
                    url: base_url + "ajax/saveFlightList",
                    data: {data: JSON.stringify(item), type: 'remove'}
                }).done(function(result) {
                    console.log(result);
                });
                return false;
            }
        });
    };
    var exist_element_in_flight_list = function(item) {
        var parent = $('#safar-flight-mylist');
        var childSelector = "div#safar-flight-mylist-content";
        var exist = false;
        parent.children(childSelector).each(function() {
            if ($(this).data('id') === item['tflight']) {
                exist = true;
            }
        });
        return exist;
    };
    $(document).on('click', '#safar-flight-result-detail', function() {
        var item = {};
        var parent = $(this).parent();
        item['tflight'] = parent.data('id');
        item['flight_number'] = parent.data('flight-number');
        item['arrival_airport'] = parent.data('arrival-airport');
        item['departure_airport'] = parent.data('departure-airport');
        item['price'] = parent.find('span#safar-flight-price').data('value');
        item['arrival_time'] = parent.find('span#safar-flight-time').data('arrival-time');
        item['departure_time'] = parent.find('span#safar-flight-time').data('departure-time');
        item['departure_date'] = parent.find('span#safar-flight-departure-date').data('value');
        item['arrival_date'] = parent.find('span#safar-flight-time').data('arrival-date');
        item['length_hour'] = parent.find('span#safar-flight-length').data('flight-hour');
        item['length_minute'] = parent.find('span#safar-flight-length').data('flight-minute');
        item['from_city'] = $('#research-from-city').val();
        item['to_city'] = $('#research-to-city').val();
        if (parent.hasClass('selected')) {
            parent.removeClass('selected');
            $(this).css('border', '1px solid #7bbeff');
            remove_from_flight_list(item);
        }
        else {
            $(this).css('border', '2px solid #7bbeff');
            parent.addClass('selected');
            if (exist_element_in_flight_list(item) === false)
                add_to_flight_list(item);
        }
    });

    $(document).on('click', '.mylist-res-close', function() {
        var item = {};
        var parent = $(this).parent().parent();
        item['tflight'] = parent.data('id');
        item['flight_number'] = parent.data('flight-number');
        item['arrival_airport'] = parent.data('arrival-airport');
        item['departure_airport'] = parent.data('departure-airport');
        item['price'] = parent.find('span#safar-flight-list-price').data('price');
        item['arrival_time'] = parent.find('span#safar-flight-list-time').data('arrival-time');
        item['departure_time'] = parent.find('span#safar-flight-list-time').data('departure-time');
        item['departure_date'] = parent.find('span#safar-flight-list-date').data('departure-date');
        item['arrival_date'] = parent.find('span#safar-flight-list-date').data('arrival-date');
        item['length_hour'] = parent.find('span#safar-flight-list-length').data('length-hour');
        item['length_minute'] = parent.find('span#safar-flight-list-length').data('length-minute');
        item['from_city'] = $('#research-from-city').val();
        item['to_city'] = $('#research-to-city').val();
        remove_from_flight_list(item);
    });

    $(document).on('click', '#safar-choose-list-save-info', function() {
        $.ajax({
            type: 'POST',
            url: base_url + "ajax/validateFlightList"
        }).done(function(result) {
            console.log(result);
            result = JSON.parse(result);
            if (result.result === 'false')
                alert(result.msg);
            else {
                //window.location = base_url + 'price/index';
                var elem = $(document);
                var $form = $('<form method="POST" action="' + base_url + 'price/index" style="display:none"></form>');
                var $child = $('<input name="child" value="' + $('#safar-flight-children').val() + '">');
                var $adult = $('<input name="adult" value="' + $('#safar-flight-adult').val() + '">');
                var $infant = $('<input name="infant" value="' + $('#safar-flight-infant').val() + '">');

                $form.append($child).append($adult).append($infant);
                $("body").append($form);
                $form.submit();
            }
            //************//
        });
    });
    $(document).on('mouseenter', '#safar-flight-result-detail', function() {
        $(this).css('cursor', 'pointer');
    });
    $(document).on('keyup', '#research-from-city,#research-to-city', function(e) {
        if (e.keyCode === 13) {
            research();
        }
    });
    $(document).on('change', '#research-from-date,#research-to-date', function() {
        research();
    });
    $(document).on('click', '#safar-sort-price-asc', function() {
        sort_flight('asc');
    });
    $(document).on('click', '#safar-sort-price-desc', function() {
        sort_flight('desc');
    });
    $(document).on('click', '#safar-flight-airline', function() {
        var code = $(this).data('code');
        var parent = $('#safar-flight');
        var childSelector = "div#safar-flight-result";
        parent.children(childSelector).each(function() {
            if ($(this).data('airline-code') !== code) {
                $(this).css('display', 'none');
            }
            else {
                $(this).css('display', 'block');
            }
        });
    });

});