<div class="result-page-mylist-flight scroll-style"> <!-- mylist -->
    <header></header>
    <form class="result-page-flight-counter">
        Adult : <button class="dec button" >-</button>
        <input value="1" id="safar-flight-adult" class="adult-counter" type="text"><button class="inc button">+</button>
        <span style="margin: 0 60px;">
            Children : <button class="dec button">-</button>
            <input value="1" id="safar-flight-children" class="children-counter" type="text"><button class="inc button">+</button>
        </span>
        Infant : <button class="dec button">-</button>
        <input value="0" class="infant-counter" id="safar-flight-infant" type="text"><button class="inc button" type="text">+</button>
    </form>
    <div id="safar-flight-mylist">
        <?php
        // print_r($my_list);
        foreach ($my_list as $list) {
            ?>
        <div class="mylist-flight-content-oneway" id="safar-flight-mylist-content" data-arrival-airport="<?= $list['arrivalAirport'] ?>" data-departure-airport="<?= $list['departureAirport'] ?>"  data-flight-number="<?= $list['flightNumber'] ?>"  data-id="<?= $list['tflight'] ?>">
                <span  style="width: 40px;"><img style="float: left;" src="<?= asset_url() ?>img/mylist-plane-icon.png"></span>
                <span id="safar-flight-list-date" data-arrival-date="<?= $list['arrival_date'] ?>" data-departure-date="<?= $list['departure_date'] ?>" style="width: 130px; line-height: 40px;"><span style="color: #009cff; width: 140px;"><?= $list['departure_date'] ?></span></span>
                <span style="width: 100px; margin-top: 5px;"><img src="<?= asset_url() ?>img/meraj-logo.png"></span>
                <span id="safar-flight-list-time" data-arrival-time="<?= $list['arrival_time'] ?>" data-departure-time="<?= $list['departure_time'] ?>" style="width: 190px; line-height: 40px;"><?= $list['departure_time'] ?> - <?= $list['arrival_time'] ?></span>
                <span id="safar-flight-list-length" data-length-hour="<?= $list['length_hour'] ?>" data-length-minute="<?= $list['length_minute'] ?>" style="width: 70px; line-height: 40px;"><?= $list['length_hour'] ?>h <?= $list['length_minute'] ?>m</span>
                <span id="" style="width: 120px; line-height: 40px;"><span style="color: #7bbeff; width: 120px;">Airbus A319</span></span>
                <span id="safar-flight-list-price" data-price="<?= $list['price'] ?>" style="width: 100px; line-height: 40px;"><?= $list['price'] ?>RI</span>
                <span style="width: 20px; line-height: 40px;"><img class="mylist-res-close" src="<?= asset_url() ?>img/mylist-res-close.png"></span>
            </div>
        <?php } ?>

        <!--    <div class="mylist-flight-content-twoway" >
             <div>
                 <span style="width: 40px;"><img style="float: left;" src="<?= asset_url() ?>img/mylist-plane-icon.png"></span>
                 <span style="width: 130px; line-height: 40px;"><span style="color: #009cff; width: 140px;">10-15-2015</span></span>
                 <span style="width: 100px; margin-top: 5px;"><img src="<?= asset_url() ?>img/meraj-logo.png"></span>
                 <span style="width: 190px; line-height: 40px;">9:30 am - 10:40 am</span>
                 <span style="width: 70px; line-height: 40px;">1h 10m</span>
                 <span style="width: 120px; line-height: 40px;"><span style="color: #7bbeff; width: 120px;">Airbus A319</span></span>
                 <span style="width: 100px; line-height: 40px;">190$</span>
             </div>
             <div>
                 <span style="width: 160px; text-align: left; padding-left: 20px; padding-top: 5px;"><img src="<?= asset_url() ?>img/round-trip-icon.png"></span>
                 <span style="width: 350px; border-top: 1px solid #178dff; border-bottom: 1px solid #178dff; padding: 5px 0; color: #178dff;">Layover in Zurich ZHR (2h 14m)</span>
                 <span style="width: 270px; text-align: left; box-sizing: border-box; padding-left: 223px;"><img class="mylist-res-close" src="<?= asset_url() ?>img/mylist-res-close.png"></span>
             </div>
             <div>
                 <span style="width: 130px; line-height: 40px; margin-left: 40px;"><span style="color: #009cff; width: 140px;">10-15-2015</span></span>
                 <span style="width: 100px; margin-top: 5px;"><img src="<?= asset_url() ?>img/meraj-logo.png"></span>
                 <span style="width: 190px; line-height: 40px;">9:30 am - 10:40 am</span>
                 <span style="width: 70px; line-height: 40px;">1h 10m</span>
                 <span style="width: 120px; line-height: 40px;"><span style="color: #7bbeff; width: 120px;">Airbus A319</span></span>
                 <span style="width: 100px; line-height: 40px;">190$</span>
             </div>
         </div>-->
    </div>
</div>
</div>