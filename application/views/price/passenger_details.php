
<div style="border: 1px solid #7bbeff; margin-top: 10px;">
    <div class="price-page-passenger-detail"> <!-- passenger-detail -->
        <header><img src="<?= asset_url(); ?>img/passenger-detail-icon.png"> Passenger Details</header>
        <div class="price-page-passenger-detail-sub-header">
            <img src="<?= asset_url(); ?>img/attention-icon.png"> 
            Please enter all passenger's info as they appear on the passenger's passport or government-issued photo identification.
        </div>
        <?php if($this->session->flashdata('errors')):?>
        <div class='price-page-passenger-detail-sub-header'> <?= $this->session->flashdata('errors') ?> </div>
        <?php endif ?>

        <form method="post" class="passenger-form" action="<?= base_url() ?>price/savePassengerInfo">

            <?php
            for ($i = 1; $i < $passengerNumber + 1; $i++) {
                ?> 
                <header>Passenger <?= $i ?></header>
                <div class="form-ctrl">
                    <div class="form-section" style="padding-left: 9px;">
                        <?= form_label('First name:*') ?></label>
                        <?= form_input(['name' => 'first_name[]', 'value' => set_value('first_name[]')]) ?>
                        <?= form_error('first_name[]', '<div class="error">', '</div>'); ?>
                    </div>
                    <div class="form-section" style="padding-left: 15px;">
                        <?= form_label('Sur name:*') ?>
                        <?= form_input(['name' => 'sur_name[]', 'value' => set_value('sur_name[]')]) ?>
                        <?= form_error('sur_name[]', '<div class="error">', '</div>'); ?>
                    </div>
                    <div class="form-section" style="padding-left: 27px;">
                        <?= form_label('B-Date:*') ?>
                        <?= form_input(['name' => 'b_date[]', 'class' => 'datepicker', 'value' => set_value('b_date[]')]) ?>
                        <?= form_error('b_date[]', '<div class="error">', '</div>'); ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="form-ctrl">
                    <div class="form-section" style="padding-left: 6px;">
                        <?= form_label('Nationality:*') ?>
                        <select id="<?= $i ?>" class="set-nat" name="nationality[]" value="<?= set_value('nationality[]') ?>">
                            <?php foreach ($countries as $country) { ?>
                                <option value="<?= $country['id'] ?>"><?= $country['en_name'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-section" style="padding-left: 17px;">
                        <?= form_label('Public ID:*') ?>
                        <?= form_input(['value' => '', 'id' => 'pub-id' . $i . '', 'class' => 'pub-id', 'name' => 'pub_id[]', 'value' => set_value('pub_id[]')]) ?>
                        <?= form_error('pub_id[]', '<div class="error">', '</div>'); ?>

                    </div>
                    <div class = "form-section" style = "padding-left: 24px;">
                        <label>Gender:* </label>
                        <select style = "width: 100px;" name = "gender[]" value="<?= set_value('gender[]s') ?>>">
                            <option>Male</option>
                            <option>Female</option>
                        </select>
                    </div>
                    <div class = "clearfix"></div>
                </div>
                <div id = "nat-info<?= $i ?>" class = "form-ctrl nat-info">
                    <div class = "form-section" style = "padding-left: 2px;">
                        <?= form_label('Passport ID:*') ?>
                        <?= form_input(['name' => 'passport_id[]', 'value' => set_value('passport_id[]')]) ?>
                        <?= form_error('passport_id[]', '<div class="error">', '</div>'); ?>

                    </div>
                    <div class = "form-section" style = "padding-left: 2px;">
                        <?= form_label('P-Issued ID:*') ?>
                        <?= form_input(['name' => 'p_issued_id[]', 'value' => set_value('p_issued_id[]')]) ?>
                        <?= form_error('p_issued_id[]', '<div class="error">', '</div>'); ?>
                    </div>
                    <div class = "form-section" style = "padding-left: 2px;">
                        <?= form_label('P-Exp Date:*') ?>
                        <?= form_input(['class' => 'datepicker', 'name' => 'p_exp_date[]', 'value' => set_value('p_exp_date[]')]) ?>
                        <?= form_error('p_exp_date[]', '<div class="error">', '</div>'); ?>                      
                    </div>
                    <div class = "clearfix"></div>
                </div>
                <img src = "<?= asset_url(); ?>img/special-assist-icon.png">
                <div class = "clearfix"></div>
                <?php
            }
            ?>
            <div class="passenger-detail-footer">
                <div class="form-ctrl">
                    <div class="form-section" style="padding-left: 23px;">
                        <?= form_label('Mobile:*') ?>
                        <?= form_input(['name' => 'mobile', 'value' => set_value('mobile')]) ?>
                        <?= form_error('mobile', '<div class="error">', '</div>') ?>
                    </div>
                    <div class="form-section" style="padding-left: 14px;">
                        <?= form_label('E-mail:*') ?>s
                        <?= form_input(['name' => 'email', 'value' => set_value('email')]) ?>
                        <?= form_error('email', '<p class="form_error">', '</p>'); ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </form>
    </div>
</div>