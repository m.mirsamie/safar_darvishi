<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Flight_model
 *
 * @author win7
 */
class Flight_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
        date_default_timezone_set("Asia/Tehran");
        $this->load->library('restWebservice');
        $this->load->model('city_model');
    }

    public function setRules() {
        $rules = [
            [
                'field' => 'from_city',
                'label' => 'From',
                'rules' => 'required|validateCity'
            ],
            [
                'field' => 'to_city',
                'label' => 'To',
                'rules' => 'required|validateCity'
            ]
        ];

        return $rules;
    }

    function searchInArray($code, $array) {

        foreach ($array as $key => $val) {
            if ($val['code'] === $code) {
                return true;
            }
        }

        return false;
    }

    public function search($fromCityIata, $toCityIata, $fromDate) {

        $data = [
            'info' => [],
            'airLines' => []
        ];
        $airLines = [];
        $webService = new RestWebservice();
        foreach ($fromCityIata as $fromIata) {
            foreach ($toCityIata as $toIata) {
                $result[] = $webService->search($fromDate, $fromIata, $toIata);
            }
        }

        for ($i = 0; $i < count($result); $i++) {
            foreach ($result[$i] as $flight) {
//                echo '<pre>';
//                print_r($flight);
//                echo '<br/>';
//                echo $flight->OperatingAirline;
//                echo '<pre>';
//                print_r($flight->FlightSegment);
//                echo '<br/>';
//                print_r($flight->FlightSegment->TotalFare->attributes()->Amount);
//                echo '<hr>';
                $airLine = (string) $flight->FlightSegment->OperatingAirline->attributes()->Code;
                $airLineName = $this->getAirLineName($airLine);
                if ($this->searchInArray($airLine, $airLines) == FALSE) {
                    $airLines[] = [
                        'name' => $airLineName,
                        'code' => $airLine
                    ];
                }
                $departureDateTime = (string) $flight->FlightSegment->attributes()->DepartureDateTime;
                $arrivalDateTime = (string) $flight->FlightSegment->attributes()->ArrivalDateTime;
                $dateTimeData = $this->getDateTimeAttribute($departureDateTime, $arrivalDateTime);
                $data['info'][] = [
                    'flightNumber' => (string) $flight->FlightSegment->attributes()->FlightNumber,
                    'tflight' => (string) $flight->FlightSegment->attributes()->TFlight,
                    'price' => (string) $flight->FlightSegment->TotalFare->attributes()->Amount,
                    'departureDate' => $dateTimeData['departureDate'],
                    'departureTime' => $dateTimeData['departureTime'],
                    'arrivalDate' => $dateTimeData['arrivalDate'],
                    'arrivalTime' => $dateTimeData['arrivalTime'],
                    'arrivalAirport' => (string) $flight->FlightSegment->ArrivalAirport->attributes()->LocationCode,
                    'departureAirport' => (string) $flight->FlightSegment->DepartureAirport->attributes()->LocationCode,
                    'flightLengthHour' => $dateTimeData['flightLengthHour'],
                    'flightLengthMinute' => $dateTimeData['flightLengthMinute'],
                    'airLineLogo' => $this->setLogo((string) $flight->FlightSegment->OperatingAirline->attributes()->Code),
                    'airLineCode' => (string) $flight->FlightSegment->OperatingAirline->attributes()->Code
                ];
            }
        }
        $data['airLines'] = $airLines;
        return $data;
    }

    public function getAirLineName($iata) {
        $this->db->select('airport')
                ->from('iata')
                ->where('iata', $iata);
        $result = $this->db->get()->row();
        return !empty($result) ? $result->airport : null;
    }

    public function getDateTimeAttribute($fromDate, $toDate) {

        $to_time = strtotime($fromDate);
        $from_time = strtotime($toDate);
        $flightLengthHour = floor(abs($to_time - $from_time) / 3600);
        $flightLengthMinute = abs($to_time - $from_time) / 60 - $flightLengthHour * 60;
        $departureDate = substr($fromDate, 0, strpos($fromDate, ' '));
        $departureTime = substr($fromDate, strpos($fromDate, ' ') + 1);
        $arrivalDate = substr($toDate, 0, strpos($toDate, ' '));
        $arrivalTime = substr($toDate, strpos($toDate, ' ') + 1);
        $departureTime = explode(':', $departureTime);
        $arrivalTime = explode(':', $arrivalTime);
        $departureHour = $departureTime[0];
        $departureMinute = $departureTime[1];
        $arrivalHour = $arrivalTime[0];
        $arrivalMinute = $arrivalTime[1];
        $departureTime = $departureHour . ':' . $departureMinute;
        $departureHour > 12 ? $departureTime.=' pm' : $departureTime.=' am';
        $arrivalTime = $arrivalHour . ':' . $arrivalMinute;
        $arrivalHour >= 12 ? $arrivalTime.=' pm' : $arrivalTime.=' am';
        $result = [
            'flightLengthHour' => $flightLengthHour,
            'flightLengthMinute' => $flightLengthMinute,
            'departureDate' => $departureDate,
            'departureTime' => $departureTime,
            'arrivalDate' => $arrivalDate,
            'arrivalTime' => $arrivalTime,
        ];

        return $result;
    }

    public function prepareData() {
        $res = $this->city_model->getAllCities();
        $fromCity = (strpos($this->input->post('from_city'), ',') != false) ? substr($this->input->post('from_city'), 0, strpos($this->input->post('from_city'), ',')) : $this->input->post('from_city');
        $toCity = (strpos($this->input->post('to_city'), ',') != false) ? substr($this->input->post('to_city'), 0, strpos($this->input->post('to_city'), ',')) : $this->input->post('to_city');
        $cities = [
            'from_city' => $fromCity,
            'to_city' => $toCity
        ];
        $fromCityIata = $this->city_model->findIataByCity($cities['from_city']);
        $toCityIata = $this->city_model->findIataByCity($cities['to_city']);
        $result = $this->flight_model->search($fromCityIata, $toCityIata, date('Y-m-d'));
        $data = [
            'flights_number' => $this->session->userdata('flight_list') ? count($this->session->userdata('flight_list')) : 0,
            'form_data' => $cities,
            'from_city' => $this->input->post('from_city'),
            'to_city' => $this->input->post('to_city'),
            'cities' => json_encode($res),
            'flight' => $result['info'],
            'air_lines' => $result['airLines'],
            'my_list' => $this->session->userdata('flight_list') ? $this->session->userdata('flight_list') : []
        ];
        return $data;
    }

    public function setLogo($code) {
        return asset_url() . "img/meraj" . /* . strtolower($code) . */"-logo.png";
    }

    public function arrayUniqueFromKey(array $arr) {
        $ret = [];
        $repeat = 'false';
        foreach ($arr as $v) {
            if (count($ret) == 0) {
                $ret[] = $v;
            } else {
                foreach ($ret as $fl) {
                    if ($v['departure_date'] == $fl['departure_date'] && $v['to_city'] == $fl['to_city'] && $v['from_city'] == $fl['from_city']) {
                        $repeat = 'true';
                        break;
                    } else {
                        $ret[] = $v;
                    }
                }
            }
        }
        return $repeat;
    }

    public function saveFlightList($type, $data) {

        $flight = json_decode($data);
        $result = [
            'price' => $flight->price,
            'arrival_time' => $flight->arrival_time,
            'departure_time' => $flight->departure_time,
            'departure_date' => $flight->departure_date,
            'arrival_date' => $flight->arrival_date,
            'length_hour' => $flight->length_hour,
            'length_minute' => $flight->length_minute,
            'tflight' => $flight->tflight,
            'flightNumber' => $flight->flight_number,
            'to_city' => $flight->to_city,
            'from_city' => $flight->from_city,
            'departureAirport' => $flight->departure_airport,
            'arrivalAirport' => $flight->arrival_airport
        ];

        $session = [];
        $add = true;
        if ($type == 'add') {
            if ($this->session->userdata('flight_list')) {
            $session = $this->session->userdata('flight_list');
            foreach ($session as $key => $val) {
                if ($val["tflight"] == $result['tflight']) {
                    $add = false;
                    break;
                }
            }
            }
            if ($add == true) {
                array_push($session, $result);
            }
        } else {
            $session = $this->session->userdata('flight_list');
            foreach ($session as $key => $val) {
                if ($val["tflight"] == $result['tflight']) {
                    unset($session[$key]);
                    $session = array_values($session);
                }
            }
        }
        $this->session->set_userdata([
            'flight_list' => $session
        ]);
    }

}
