
<div class="price-page-caution"> <!-- caution -->
    <header>Use caution when packing</header>
    <p>
        Some everyday products, like electronic cigarettes and aerosol spray starch, can be dangerous when transported on the aircraft in carry-on and/or checked baggage. Changes in temperature or pressure can cause some items to leak, generate toxic fumes or start a fire. Carriage of prohibited items may result in fines or in certain cases imprisonment. Please ensure there are no forbidden hazardous materials in your baggage like:
    </p>
    <ul>
        <li><img src="<?= asset_url(); ?>img/cation1.png"></li>
        <li><img src="<?= asset_url(); ?>img/cation2.png"></li>
        <li><img src="<?= asset_url(); ?>img/cation3.png"></li>
        <li><img src="<?= asset_url(); ?>img/cation4.png"></li>
        <li><img src="<?= asset_url(); ?>img/cation5.png"></li>
        <li><img src="<?= asset_url(); ?>img/cation6.png"></li>
        <li><img src="<?= asset_url(); ?>img/cation7.png"></li>
    </ul>
</div>