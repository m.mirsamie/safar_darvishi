$(document).ready(function() {
    //datepicker
    $(".datepicker").datepicker({
    });

    //choose-page submitting form
    $(document).ready(function() {
        var spanSubmit = $('.choose-page-search form div span');
        spanSubmit.on('click', function() {
            $(this).closest('form').submit();
        });
    });

    //result-filter
    $('.result-page-box ul li span').hide();
    $('.result-page-box ul li').mouseover(function() {
        $(this).find('span').stop().slideDown('slow');
    });
    $('.result-page-box ul li').mouseout(function() {
        $(this).find('span').stop().slideUp('slow');
    });

    //result-detail
    $('.result-page-toggle').hide();
    $('.result-btn').click(function() {
        $(this).parent().parent().find(".result-page-toggle").stop().toggle('slow');
    });

    //result-detail-btn
    $('.result-btn').click(function() {
        var _this = $(this);
        var current = _this.attr("src");
        var swap = _this.attr("data-swap");
        _this.attr('src', swap).attr("data-swap", current);
    });
});